import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import './login.sass';

const Login = props => {
    const {
        isLoggin = true,
        userName = 'Valeria'
    } = props

    return (
        <li className="login">
            {isLoggin
                ? <Fragment>
                    <div id="userInformation" ><span>Bienvenido</span> {userName}</div>
                    <div className="box__login" id="box__login">
                        <dl>
                            <dt>
                                <Link to="/mi-cuenta/mis-datos/">Entrar a mi cuenta</Link>
                            </dt>
                            <dt>
                                <Link to="/mi-cuenta/mis-pedidos/1/estatus/2/">Revisar mis pedidos</Link>
                            </dt>
                        </dl>
                        <dl className="logOut">
                            <dt>
                                <p>¿ No eres {userName} ?</p>
                                <div className="cerrar">Cerrar mi sesión</div>
                            </dt>
                        </dl>
                    </div>
                </Fragment>
                
                : <Fragment>
                    <div id="userInformation"><span>Hola</span> Regístrate</div>
                    <div className="box__login" id="box__login">
                        <dl className="logIn">
                            <dt>
                                <Link className="redBtn btn-login" to="/login">Ingresar</Link>
                            </dt>
                            <dd>
                                <p>¿Eres Nuevo?</p>
                                <Link className="standardBtn btn-registro" to="/loginregistro">Regístrate</Link>
                            </dd>
                        </dl>
                    </div>
                </Fragment>
            }
        </li>
    )
}

export default Login;