import React from 'react';
import { Link } from 'react-router-dom';
import './cart.sass';

const Cart = props => {
    return (
        <li className="cart notification cartPreview" id="cantidadProductos">
            <span className="push" id="cart_count">1</span>
            <Link to="/carrito/detalle" id="a"><span>Mi </span>carrito</Link>
            <div className="box__carrito">
                <h3>Mi Carrito</h3>

                <div className="boxCarritoCont">
                    <article className="caja__product">
                        <div className="fila_producto">
                            <div className="bcp_producto">

                                <div className="contImg">
                                    <Link to="/producto/494603/agua-natural">
                                        <img src="https://resources.claroshop.com/imagenes-sanborns-ii/1200/758104001712.jpg" alt="producto" />
                                    </Link>
                                </div>

                                <div className="contInfo">
                                    <Link to="/producto/494603/agua-natural">
                                        <p className="producto">Agua Natural</p>
                                    </Link>
                                    <div className="info">
                                        <p>Vendido por: Sanborns</p>
                                        <p>cantidad: 1 </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>

                <div className="botones_cajaPago">
                    <div className="botones">
                        <Link to="/carrito/detalle" className="botonComprarClick">Ir al carrito</Link>
                        <Link to="/carrito/valida-caja" id="botonComprarPreview" className="botonComprarAhora">Comprar Ahora</Link>
                    </div>
                </div>

            </div>
        </li>
    )
}

export default Cart;