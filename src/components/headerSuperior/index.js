import React, { Component } from 'react';
import Header from './header';
import './header.sass';
import { connect } from 'react-redux';
import mostrarMenu1 from '../../redux/actions/mostrarMenuResponsive';

class Index extends Component {
    render() {
        return (
            <Header onResult={this.props.onResult} handleMostrar={this.props.mostrarMenu}
                bandera={this.props.mostrarMenuResponsive} />
        )
    }
}

const mapStateToProps = (state) => {
    return {
        mostrarMenuResponsive: state.mostrarMenuResponsive
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        mostrarMenu: () => dispatch(mostrarMenu1()),
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Index)