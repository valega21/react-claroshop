import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import './wishlist.sass';

const Wishlist = props => {
    const {
        isLoggin = true
    } = props

    return (
        <Fragment>
            {
                isLoggin ?
                    <li className="wishlist notification" >
                    {/* id="wishlist_snippet"> */}
                        <span className="push">0</span>
                        <Link to="/mi-cuenta/listadeseos" className="icono">
                            <span>Mi Lista</span>de deseos
                        </Link>
                    </li>
                    :
                    null
            }
        </Fragment>
    )
}

export default Wishlist;