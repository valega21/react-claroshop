import React from 'react';
import { Link } from 'react-router-dom';
import Search from './search/search';
import WishlistC from './wishlist';
import LoginC from './login';
import CartC from './cart';

const Header = ({ handleMostrar, onResult, bandera }) => {
    return (
        <div className="header_superior">
            <div className="container2">
                <Link to="/" title="Ir a la Página principal de Claroshop" rel="follow" className="header_logo"></Link>

                <div className={bandera === true ? "desktopMenu close" : "desktopMenu "} onClick={handleMostrar}></div>

                <Search onResult={onResult} />

                <nav className="menuPrincipal menuAnim">
                    <ul>
                        <WishlistC />
                        <LoginC />
                        <CartC />
                    </ul>
                </nav>
            </div>
        </div>
    )
}

export default Header;