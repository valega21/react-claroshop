import React, { Component } from 'react';
import './search.sass';
const API_KEY = '53de9c96';

class Search extends Component {
    state = {
        inputMovie: ''
    }

    handleChange = (event) => {
        this.setState({ inputMovie: event.target.value })
    }

    handelSubmit = (e) => {
        e.preventDefault();
        var url = `http://www.omdbapi.com/?apikey=${API_KEY}&s=${this.state.inputMovie}`
        fetch(url)
            .then(respuesta => respuesta.json())
            .then(results => {
                const { Search = [] } = results
                console.log(Search)
                this.props.onResult(Search)
            })
    }

    render() {
        return (
            <div  className="anteanter_section search">
            <form onSubmit={this.handelSubmit} className="formulario">
                <section className="navbar-form navbar-left hpadding0 hmargecontenidozul">
                    <div className="contSearch suggestSearch">
                        <div className="input2">
                            <input id="search" name="search" className="input" type="search" placeholder="¿Qué es lo que buscas?" onChange={this.handleChange} aria-label="Campo de busqueda" />
                            <button className="bt__search" aria-label="Boton de busqueda"></button>
                        </div>
                    </div>
                </section>
            </form>
            </div>
        )
    }
}
export default Search;