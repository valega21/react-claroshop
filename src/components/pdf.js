import React, { Component, Fragment } from 'react';
// import logo from '../logo.svg';
import Pdf from "react-to-pdf";
import './pdf.sass'

class Generar extends Component {
    render() {
        const ref = React.createRef();
        const options = {
            orientation: 'portrait',
            textAlign: 'center',
            padding: '0px',
            top: '-20px'

        };
        return (
            <Fragment>
            
                <div ref={ref} className="pdf" size='A4'>
                    {/* <div className=""> */}
                    <div className="seccionPrincipal">
                        <p className=""> PEDIDO NO. no Pedido</p>
                        <p className=""> Fecha de Compra:  DD/MM/YYYY </p>
                        <p className=""> Fecha de Límite de pago: DD/MM/YYYY</p>
                    </div>
                    {/* </div> */}

                    {/* <Fragment>
                        <div className="displayFlex marginL20 logo">
                            <div className="width50 alignLeft">
                                <img className="" src={logo} alt="oxxo" style={{ width: '10%' }}></img>
                            </div>
                            <div className="width50 alignRight">
                                <p className="bold">Total a pagar</p>
                                <p className="bold2"> $ <span> valor</span></p>
                            </div>
                        </div>

                        <div className="fondo  paddingT contenedorReferencia">
                            <p className="bold2 paddingB10">REFERENCIA</p>
                            <div className="width60 bordeReferencia ancho marginL30 numReferencia"><span> XXXX - XXXX - XXXX - XX</span></div>
                        </div>

                        <div className="">
                            <p className="bold"> Instrucciones</p>
                            <div className="">
                                <p className="paddingB5">1. Acude a la tienda OXXO más cercana. </p>
                                <p className="paddingB5">2. Indica en caja que quieres realizar un pago de OXXOPay.</p>
                                <p className="paddingB5">3. Dicta al cajero el número de referencia en esta ficha para que tecleé directamente en la pantalla de venta.</p>
                                <p className="paddingB5">4. Realiza el pago conrrespondiente con dinero en efectivo.</p>
                                <p className="paddingB5">5. Al confirma tu pago, el cajero te entregará un comprobante impreso. En el podrás verificar que se haya realizado
                            correctamente. Conserva este comprobante de pago.</p>

                                <p className="paddingT">Al completar estos pasos recibirás un correo confirmando tu pago de manera inmediata.</p>
                                <p className="paddingT alignCenter">OXXO cobrará una comisión adicional al momento de realizar el pago.</p>
                            </div>
                        </div>
                    </Fragment> */}
                </div>
                <Pdf targetRef={ref} filename="ficha-de-pago.pdf" options={options} style={{top:'-40px'}}>
                    {/* scale={0.8} x={.5} y={.5} */}
                    {({ toPdf }) => <button onClick={toPdf}>Descargar ficha de pago</button>}
                </Pdf>
            </Fragment>
        )
    }
}

export default Generar;