import React, { Component, Fragment } from 'react';
import HeaderInferior from '../headerInferior/inferior';
import HeaderSuperior from '../headerSuperior';

class Header extends Component {
    render() {
        return (
            <Fragment>
                <HeaderSuperior  onResult={this.props.onResult} />
                <HeaderInferior />
            </Fragment>
        )
    }
}

export default Header;