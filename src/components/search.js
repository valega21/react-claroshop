import React, { Component } from 'react';

const API_KEY = '53de9c96';

class Search extends Component {
    state = {
        inputMovie: ''
    }

    handleChange = (event) => {
        this.setState({ inputMovie: event.target.value })
    }

    handelSubmit = (e) => {
        e.preventDefault();
        var url = `http://www.omdbapi.com/?apikey=${API_KEY}&s=${this.state.inputMovie}`
        fetch(url)
            .then(respuesta => respuesta.json())
            .then(results => {
                const { Search=[], totalResults="0" } = results
                console.log(Search)
                this.props.onResults(Search)
            })
    }

    render() {
        return (
            <form onSubmit={this.handelSubmit}>
                <div className="field has-addons">
                    <div className="control">
                        <input className="input" type="text" placeholder="Buscar elemento..." onChange={this.handleChange} />
                    </div>
                    <div className="control">
                        <button className="button is-info"> Buscar </button>
                    </div>
                </div>
            </form>
        )
    }
}
export default Search;