import React from 'react';
import { Link } from 'react-router-dom';

const Movie = ({ id, imagen, titulo, anio }) => {
    console.log("id: ", id)
    return (
        <Link to={`/detail/${id}`} className="card">
            <div className="card-image">
                <figure className="image">
                    <img src={imagen} alt="poster" />
                </figure>
            </div>
            <div className="card-content">
                <div className="media">
                    <div className="media-content">
                        <p className="title is-4">{titulo}</p>
                        <p className="subtitle is-6">{anio}</p>
                    </div>
                </div>

            </div>
        </Link>
    )
}

export default Movie;