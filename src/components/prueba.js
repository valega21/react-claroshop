import React, { Component } from 'react';
import { CopyToClipboard } from 'react-copy-to-clipboard';

class Prueba extends Component {
    state = {
        value: '{no Pedido}',
        copied: false,
        click: false
    };

    onCopy = () => {
        this.setState({ copied: true })
    }

    handleClick = () => {
        this.setState({ click: !this.state.click })
        setTimeout(this.setState.bind(this, { click: false }), 500);
    }

    handleChange = (e) => {
        const valor = e.target.value;
        this.setState({ value: valor, copied: false })
    }
    
    render() {
        return (
            <div className="pedido">
                <CopyToClipboard text={this.state.value}
                    onCopy={() => this.setState({ copied: true })}>
                    <span className="texto">Número de Pedido: </span>
                </CopyToClipboard>

                <input className="entrada" type="text" value={this.state.value} disabled onChange={this.handleChange} />

                <CopyToClipboard text={this.state.value} onCopy={() => this.onCopy()}>
                    <button onClick={this.handleClick} className={this.state.click ? "ayuda tooltip bottom" : "ayuda"}>
                        <span className="tiptext">Se ha copiado el número de pedido.</span>
                    </button>
                </CopyToClipboard>
            </div>
        )
    }
}

export default Prueba;