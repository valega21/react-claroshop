import React from 'react';
import './contacto.sass';

const ContactoTiendas = () => {
    return (
        <section className="mod__AyudaContacto">
            <div className="container">
                <h2>Contáctanos</h2>
                <p>Si tienes alguna o comentario:</p>
                <div className="cont_Card">
                    <div className="card">
                        <h3>Teléfonos de contacto</h3>
                        <div className="icon-tel"></div>
                        <p>CDMX y Área Metropolitana:
                            <span><strong>01 800 890 65 66</strong></span>
                        </p>

                    </div>

                    <div className="card">
                        <h3>Escríbenos</h3>
                        <div className="icon-mail"></div>
                        <p>Escríbenos tus consultas, dudas o comentarios y te daremos una respuesta a la brevedad</p>

                        <a className="link" href="mailto:clientes@claroshop.com">Escríbenos</a>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default ContactoTiendas;