import React, { Component } from 'react';
import './servicios.sass';

const Articulo = ({ empresa, claseLogo, children }) => {
    return (
        <article>
            <div className="servGridElement">
                <div className="marca">
                    <span className={claseLogo}>{empresa}</span>
                </div>
                <ul>
                    {children}
                </ul>
            </div>
        </article >
    )
}

class ServiciosComponente extends Component {
    render() {
        return (
            <div className="serviciosGridCont">
                <Articulo empresa="Telcel" claseLogo="logoTelcel">
                    <li><a href="https://recargas.claroshop.com/" target="_blank" rel="noopener noreferrer">Recarga Amigo</a></li>
                    <li><a href="https://www.mitelcel.com/mitelcel/login" target="_blank" rel="noopener noreferrer">Ver Facturas</a></li>
                    <li><a href="https://www.telcel.com/personas/telefonia/planes-de-renta/todavia-no-tienes-plan#!equipos?mid=1200" target="_blank" rel="noopener noreferrer">Planes Tarifarios</a></li>
                </Articulo>
                <Articulo empresa="Telmex" claseLogo="logoTelmex">
                    <li><a href="https://commerce.telmex.com/pagoenlinea/datosLineaExpressND.xhtml?_ga=1.4740080.2116568688.1481125607#arriba" target="_blank" rel="noopener noreferrer">Pago Express</a></li>
                    <li><a href="http://www.telmex.com/web/hogar/servicios" target="_blank" rel="noopener noreferrer">Ver Servicios</a></li>
                </Articulo>
                <Articulo empresa="Claro" claseLogo="logoClaro">
                    <li><a href="https://www.claromusica.com/intro" target="_blank" rel="noopener noreferrer">Música</a></li>
                    <li><a href="https://www.clarovideo.com/mexico/homeuser" target="_blank" rel="noopener noreferrer">Video</a></li>
                </Articulo>
                <Articulo empresa="Inbursa" claseLogo="logoInbursa">
                    <li><a href="https://cotizatuseguro.inbursa.com:4041/negocio-quimera/mvc/others/autotalPublico/init?ca=PUBLICO&pa=P" target="_blank" rel="noopener noreferrer">Autotal</a></li>
                </Articulo>
                <Articulo empresa="Sanborns" claseLogo="logoSanborns">
                    <li><a href="http://librosdigitales.sanborns.com.mx/" target="_blank" rel="noopener noreferrer">Libros Digitales</a></li>
                    <li><a href="https://www.sanborns.com.mx/categoria/50202/digitales/" target="_blank" rel="noopener noreferrer">Revistas Digitales</a></li>
                </Articulo>
            </div>
        )
    }
}

export default ServiciosComponente;