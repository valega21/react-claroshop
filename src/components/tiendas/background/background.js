import React from 'react';
import Chosen from '../buscador/buscador'
import './background.sass'
import { Component } from 'react';

class Background extends Component {
    constructor(props) {
        super(props);

        this.state = {
            event: ""
        };
    }

    onChange = (evento) => {
        var reemplazo = evento.replace(/ /g, "").replace(/%/g, '-')
        this.setState({ event: reemplazo });
        console.log("value select: ", this.state.event)
        if (evento !== "") {
            window.location.href = reemplazo
        }
    }

    render() {
        const { url, todas, flag } = this.props

        const Fondo = {
            backgroundImage: `url(${url})`
        }

        const tiendaIndice = []

        todas.map((obj, i) => {
            var carru = Object.entries(obj)
            return (carru.map((ob, j) => {
                tiendaIndice.push({ valor: ob[1] })
            }))
        })

        return (
            <div className="tiendasHeader" style={Fondo} >
                {/* <img src={url} alt="background" >
                </img> */}

                <div className={flag === 2 ? "container todosServicios" : 'todosServiciosOcultar'}>
                    <h2>Servicios</h2>
                    <p>Contamos con una amplia gama de servicios que ofrecen nuestras marcas</p>
                </div>

                <div className={flag === 1 ? "container tiendasOficialesTiendas" : 'ocultarTiendas'}>
                    <h2>Tiendas Oficiales</h2>
                    <p>Tus marcas preferidas ya están en  Claroshop</p>


                    <div className="tiendasSearch">
                        {this.redireccion}
                        <Chosen datanoResultsText="No se encontraron resultados" className="chosen-container chosen-container-single" todas={todas} onChange={this.onChange} >
                            <option value=""></option>
                            {tiendaIndice.map((tiendas, i) => {
                                return (tiendas.valor.map(name => {
                                    return (
                                        <option key={name.id} value={"/tienda/" + name.id + "/" + name.nombre}> {name.nombre}</option>
                                    )
                                }))
                            })}
                        </Chosen>
                    </div>
                </div>
            </div >
        )
    }
}

export default Background;