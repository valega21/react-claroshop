import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import tiendasData from '../../redux/actions/getTiendas';
import Background from './background/background';
import './tiendas.sass'
import Destacadas from './destacadas/destacadas';
import TodasTiendasOficiales from './todas/todas';
import ContactoTiendas from './contacto/contacto';
import { Link } from 'react-router-dom';
import $ from 'jquery'

class TiendaComponent extends Component {
    constructor() {
        super();
        this.subir = React.createRef();
    }
    componentDidMount() {
        this.props.tiendasData();
    }

    ocultarYmostrarAlHacerscroll = () => {
        $(document).ready(function () {

            $('#boton-ir-arriba').click(function () {
                $('body, html').animate({
                    scrollTop: '0px'
                }, 500);
            });

            $(window).scroll(function () {

                if ($(this).scrollTop() > 0) {
                    $('#boton-ir-arriba').slideDown(300);

                }
                else {
                    $('#boton-ir-arriba').slideUp(300);
                }
            });
        });
    }

    verBotonSubirPagina = () => {
        var elemento = document.getElementById('boton-ir-arriba');
        const posicion = elemento.getBoundingClientRect();

        if (posicion.top < 570) {
            $(document).ready(function () {
                var myElement = $("#boton-ir-arriba");
                myElement.css("display", "none");
            });
        }
    }


    render() {
        const { listaTiendas } = this.props
        const lista = Object.values(Object.assign({}, listaTiendas))
        const arreglo = []
        const destacadas = []
        const oficiales = []

        lista.map((ar, i) => {
            var lis = Object.entries(ar)
            lis.map((item, j) => {
                return (item[i] === "background_image"
                    ? arreglo.push(item[1])
                    : null
                )
            })
        })

        lista.map((ar, i) => {
            var lis = Object.entries(ar)
            lis.map((item, j) => {
                return (item[i] === "destacadas"
                    ? destacadas.push(item[1])
                    : null
                )
            })
        })

        lista.map((ar, i) => {
            var lis = Object.entries(ar)
            lis.map((item, j) => {
                return (item[i] === "tiendas"
                    ? oficiales.push(item[1])
                    : null
                )
            })
        })

        return (
            <Fragment>
                <section className="sec__tiendasGrid border">
                    <Background url={arreglo[0]} todas={oficiales} ref={this.subir} />

                    <div className="seccionTiendas">
                        <Destacadas tiendasDestacadas={destacadas[0]} />

                        <TodasTiendasOficiales todas={oficiales} />
                    </div>
                </section>

                <ContactoTiendas />

                <Link id="boton-ir-arriba" style={{ display: 'inline' }} 
                onClick={this.ocultarYmostrarAlHacerscroll(this.subir)}></Link>
            </Fragment>

        )
    }
}

const mapStateToProps = (state) => {
    return {
        listaTiendas: state.reducerTiendas
    }
}

const mapDispatchToProps = {
    tiendasData
}

export default connect(mapStateToProps, mapDispatchToProps)(TiendaComponent);