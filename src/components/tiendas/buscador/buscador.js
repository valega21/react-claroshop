import React, { Component } from 'react'
import $ from 'jquery'
import "chosen-js/chosen.css"
import "chosen-js/chosen.jquery.js"

export default class Chosen extends Component {
    constructor() {
        super();
        this.state = {
            event: 0
        };
    }

    componentDidMount() {
        this.$el = $(this.el);
        this.$el.chosen();

        this.handleChange = this.handleChange.bind(this);
        this.$el.on('change', this.handleChange);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.children !== this.props.children) {
            this.$el.trigger("chosen:updated");
        }
    }

    componentWillUnmount() {
        this.$el.off('change', this.handleChange);
        this.$el.chosen('destroy');
    }

    handleChange = (e) => {
        this.props.onChange(e.target.value);
    }

    render() {
        return (
            <div style={{ position: 'relative', bottom: '20px', with: "auto" }}>
                <select data-placeholder="Busca aquí tu tienda preferida" noresultstext="No se encontraron resultados"
                    className="Chosen-select" ref={el => this.el = el} value={this.state.value} onChange={this.handleChange}>
                    {this.props.children}
                </select>
            </div>
        );
    }
}