import React, { Fragment, Component } from 'react';
import './todas.sass';
import { Link } from 'react-router-dom';

const Listado = ({ tiendaIndice, letra }) => {
    return (
        <div className="tiendasLetter">
            {tiendaIndice.map((tienda, index) => {
                return (
                    (tienda.indice === letra
                        ?
                        <Fragment key={index} >
                            <h3 id={tienda.indice}  > <span id={"letra" + index}>{tienda.indice}</span></h3>
                            <div className="tiendasLetterCont">
                                {tienda.valor.map((valor, i) => {
                                    return (
                                        <article key={i}>
                                            <Link to={"/tienda/" + valor.id + "/" + valor.nombre.replace(/ /g, "").replace(/%/g, '-')} >
                                                <img src={valor.logo} alt={valor.nombre} />
                                            </Link>
                                        </article>
                                    )
                                })}
                            </div>
                        </Fragment>
                        : null
                    )
                )
            })}
        </div>
    )
}

class TodasTiendasOficiales extends Component {
    constructor(props) {
        super(props);

        this.state = {};
        this.a = React.createRef()
    }

    render() {
        const { todas } = this.props;

        // console.log("-> all: ", todas[0]);
        // console.log("referencia: ", this.a)
        const tiendaIndice = []

        todas.map((obj, i) => {
            var carru = Object.entries(obj)
            return (carru.map((ob, j) => {
                tiendaIndice.push({ indice: ob[0], valor: ob[1] })
            }))

        })

        return (
            <div className="tiendasOthers">
                <div className="listOfLetters">
                    <div className="container">
                        <ul>
                            {tiendaIndice.sort((a, b) => a.indice.localeCompare(b.indice)).map((letra, index) => {
                                return (
                                    ((letra.indice !== String(1)) && (letra.indice !== String(2)) && (letra.indice !== String(3)))
                                        ?
                                        < li key={index} >
                                            <a className="activeLetter" href={"#" + letra.indice} >{letra.indice}</a>
                                        </li>
                                        : null
                                )
                            })}
                        </ul>


                        <Fragment>
                            <h2>Tiendas Oficiales</h2>
                            {tiendaIndice.sort((a, b) => a.indice.localeCompare(b.indice)).map(letra => {
                                return (
                                    <Listado tiendaIndice={tiendaIndice} letra={letra.indice} key={letra.indice} />
                                )
                            })}
                        </Fragment>

                    </div>
                </div>

            </div >
        )
    }
}

export default TodasTiendasOficiales;