import React from 'react';
import './destacadas.sass';
import { Link } from 'react-router-dom';

const Destacadas = ({ tiendasDestacadas }) => {

    const store = Object.values(Object.assign({}, tiendasDestacadas))

    return (
        <div className="tiendasTop">
            <div className="container">

                <h2> Tiendas Destacadas</h2>

                {store.map((tienda, i) => {
                    return (
                        <article key={i}>
                            <Link to={"/Tienda/" + tienda.id + "/" + tienda.nombre + "/"}>
                                <img src={tienda.url_imagen} alt={tienda.nombre} />
                            </Link>
                        </article>
                    )
                })}

            </div>
        </div>
    )
}

export default Destacadas;