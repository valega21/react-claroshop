import React, { Component } from 'react';
import Movie from './movie'

class MovieList extends Component {
    render() {
        const { results } = this.props;
        return (
            <div className="movieList">
                {results.map(movie => {
                    return (
                        <div className="item" key={movie.imdbID}>
                            <Movie  titulo={movie.Title} anio={movie.Year} imagen={movie.Poster} id={movie.imdbID}/>
                        </div>
                    )
                })
                }
            </div>

        )
    }
}
export default MovieList;