import React from 'react';
import './tiendasOficiales.sass';
import Enlace from './link';

const Tiendas = props => {
    return (
        <nav className="menuTiendas noSubMenu">
            <ul>
                <Enlace enlace='/tiendas' title="Tiendas Oficiales" clase="tiendas alone" />
                <Enlace enlace='/l/faqs/' title="¿Necesitas Ayuda?" clase="mall alone" />
            </ul>
        </nav>
    )
}

export default Tiendas;