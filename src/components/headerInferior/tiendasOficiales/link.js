import React from 'react';
import { Link } from 'react-router-dom';

const Enlace = ({clase, title, estilo, enlace}) => {
    return (
        <li className={clase} style={{width:'50%'}}>
            <Link to={enlace} className={estilo}>{title}</Link>
        </li>
    )
}

export default Enlace;