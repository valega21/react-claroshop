import React, { Component } from 'react';
import './promo.sass';
import Enlace from '../tiendasOficiales/link';

class Promociones extends Component {

    render() {
        return (
            <nav className="promoHeader">
                <ul>
                    <Enlace title="Ofertas" estilo="menuOferta" enlace="/#"/>
                    <Enlace title="Gamers" enlace="/#"/>
                    <Enlace title="Supermercado" estilo="super" enlace="/#"/>
                    <Enlace title="Comprar a Mayoreo" enlace="/#"/>
                </ul>
            </nav>
        )
    }
}

export default Promociones;