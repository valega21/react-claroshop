import React, { Component } from 'react';
import './headerMovil.sass';
import { Link } from 'react-router-dom';

const EnlaceMovil = ({ link, estilo, icono, titulo }) => {
    return (
        <Link to={link} className={estilo}>
            {titulo} <i className={icono} />
        </Link>
    )
}

const Especialidades = ({ titulo, linkOfertas, linkCelulares, estilo, handleEspecialidad, mostrarEspecialidad }) => {
    return (
        <dl className={mostrarEspecialidad ? "navRecomendadosMovil show " : "navRecomendadosMovil"} onClick={handleEspecialidad}>
            <dt>{titulo} <i className={estilo}> </i></dt>
            <dd>
                <dl>
                    <dt>
                        <Link to={linkOfertas}> Ofertas</Link>
                    </dt>
                </dl>
                <dl>
                    <dt>
                        <Link to={linkCelulares}> Celulares</Link>
                    </dt>
                </dl>
            </dd>
        </dl>
    )
}

export default class HeaderMovil extends Component {

    constructor(props) {
        super(props);
        this.state = {
            mostrarEspecialidad: false,
        }
    }

    handleEspecialidad = () => {
        this.setState({
            mostrarEspecialidad: !this.state.mostrarEspecialidad
        })
    }

    render() {
        const { mostrarEspecialidad } = this.state

        return (
            <section className={this.props.bandera === true ? "header__movil view" : "header__movil"} onClick={this.handleMenu}>
                <nav className="navegacionM">
                    <EnlaceMovil link="/#" estilo="movil categorias" icono="fa fa-chevron-right" titulo="Ver Categorias" />
                    <EnlaceMovil link="/#" estilo="movil tiendas" icono="fa fa-chevron-right" titulo="Tiendas" />
                    <EnlaceMovil link="/#" estilo="movil servicios" icono="fa fa-chevron-right" titulo="Servicios" />
                    <EnlaceMovil link="/#" estilo="movil ayuda" icono="fa fa-chevron-right" titulo="¿Necesitas ayuda?" />
                    <Especialidades titulo="Especialidades" linkOfertas="/#" linkCelulares="/#"
                        mostrarEspecialidad={mostrarEspecialidad}
                        handleEspecialidad={this.handleEspecialidad} />
                    <EnlaceMovil link="/#" estilo="movil supermercado" icono="fa fa-chevron-right" titulo="Supermercado" />
                    <EnlaceMovil link="/#" estilo="movil mayoreo" icono="fa fa-chevron-right" titulo="Comprar a Mayoreo" />
                </nav>
            </section>
        )
    }
}