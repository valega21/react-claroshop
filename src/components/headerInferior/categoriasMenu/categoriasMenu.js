import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import './categoriasMenu.sass';

const CategoriasMenu = props => {
    const {
        categoriasMenu
    } = props

    const menuCategoriasData = categoriasMenu.map((firstLevel, index) =>
        <Fragment key={index}>
            {(firstLevel.estatus && firstLevel.visible) ?
                <dl className="menuFirstLevel">
                    <dt>
                        <Link to={'/categoria/' + firstLevel.id + '/' + firstLevel.seo + '/'} className={'ico-' + firstLevel.seo} aria-label={firstLevel.nombre} >{firstLevel.nombre}</Link>
                        <dl className="menuSecondLevel">
                            <dt>
                                <dl className="menuThirdLevel">
                                    {
                                        (typeof (firstLevel.subcategorias) == 'object') ?
                                            <Fragment>
                                                {
                                                    firstLevel.subcategorias.map((secondLevel, secondIndex) =>
                                                        <Fragment key={secondIndex}>
                                                            {(secondLevel.estatus && secondLevel.visible) ?
                                                                <dt className="more">
                                                                    <Link to={'/categoria/' + secondLevel.id + '/' + secondLevel.seo + '/'} aria-label={secondLevel.nombre} >{secondLevel.nombre}</Link>
                                                                    <ul>
                                                                        {
                                                                            (typeof (secondLevel.subcategorias) == 'object') ?
                                                                                <Fragment>
                                                                                    {
                                                                                        secondLevel.subcategorias.map((thirdLevel, thirdIndex) =>
                                                                                            <Fragment key={thirdIndex}>
                                                                                                {(thirdLevel.estatus && thirdLevel.visible) ?
                                                                                                    <li className="alone">
                                                                                                        <Link to={'/categoria/' + thirdLevel.id + '/' + thirdLevel.seo + '/'} aria-label={thirdLevel.nombre}>{thirdLevel.nombre}</Link>
                                                                                                    </li>
                                                                                                    :
                                                                                                    null
                                                                                                }
                                                                                            </Fragment>
                                                                                        )
                                                                                    }
                                                                                </Fragment>
                                                                                :
                                                                                null
                                                                        }
                                                                    </ul>
                                                                </dt>
                                                                :
                                                                null
                                                            }

                                                        </Fragment>
                                                    )
                                                }
                                            </Fragment>
                                            :
                                            null
                                    }
                                </dl>
                            </dt>
                        </dl>
                    </dt>
                </dl>
                :
                null
            }
        </Fragment>
    );


    return (
        <nav className="productosHead">
            <ul>
                <li className="productosNewCat hide">

                    <Link to="/l/categorias/" aria-label="Ver Categorías">Ver Categorías</Link>

                    <div className="menuHeader">
                        <div className="subMenu">

                            {menuCategoriasData}

                        </div>
                    </div>
                </li>
            </ul>
        </nav>
    )
}

export default CategoriasMenu;