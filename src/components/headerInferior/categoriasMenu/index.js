import React, { Component } from 'react';
import { connect } from 'react-redux';
import CategoriasMenu from './categoriasMenu';

import menuPortalData from '../../../redux/actions/getMenu.action';

class MenuCategory extends Component {

    constructor(props) {
        super(props);
        this.state = this.props.menuPortalData()
    }

    render() {
        return (
            <CategoriasMenu
                categoriasMenu={this.props.menuCategoryHeader.menuCategoryHeader}
            ></CategoriasMenu>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        menuCategoryHeader: state.menuCategoryHeader
    }
}

const mapDispatchToProps = {
    menuPortalData
}

export default connect(mapStateToProps, mapDispatchToProps)(MenuCategory); 