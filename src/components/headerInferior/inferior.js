import React, { Component, Fragment } from 'react';
import MenuCategory from '../headerInferior/categoriasMenu';
import TiendasOficiales from '../headerInferior/tiendasOficiales';
import Promociones from './promo';
import './inferior.sass';
import HeaderMovil from './headerMovil/headerMovil';
import { connect } from 'react-redux'

class HeaderInferior extends Component {

    render() {
        return (
            <Fragment>
                <div className="header_inferior">
                    <div className="containerInferior">
                        <MenuCategory />
                        <Promociones />
                        <TiendasOficiales />
                    </div>
                </div>

                <HeaderMovil bandera={this.props.mostrarMenuResponsive} />
            </Fragment>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        mostrarMenuResponsive: state.mostrarMenuResponsive
    }
}

export default connect(mapStateToProps)(HeaderInferior);