import React, { Component, Fragment } from 'react';

class Pasos extends Component {
    render() {
        return (
            <Fragment>
                <ul>
                    <li> <span className=""></span> Pedido el </li>
                    <li> <span className=""></span> Autorizado </li>
                    <li> <span className=""></span> Preparando tu Producto</li>
                    <li> <span className=""></span> Enviado el </li>
                    <li> <span className=""></span> En camino </li>
                    <li> <span className=""></span> Llegando</li>
                </ul>
            </Fragment>
        );
    }
}

export default Pasos;