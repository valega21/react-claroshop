import React, { useState, useEffect, Fragment } from 'react';
import getSearch from '../../redux/actions/getBuscadorAction'
import { Link } from 'react-router-dom';
import { PRODUCTS_PER_PAGINATION, NO_IMAGE, HIDDEN_DISCOUNT, ENVIO_GRATIS } from '../../constants/endPoints';
import './busquedaProducto.sass'
import BotonWishlist from './botonWishlist';
import CurrencyFormat from "react-currency-format"
import $ from 'jquery'

const Producto = ({ ...props }) => {

    const { id, nombre, nombreSEO, precio, precioLista, foto, descuento, shipping_price } = props

    return (
        <article className="cardProduct">
            <Link to={`/producto/${id}/${nombreSEO != null ? nombreSEO : nombre.replace(/ /ig, '-')}/`} title={nombreSEO}>

                <div className="contImgProd">
                    <img loading="lazy" src={foto != null ? foto : NO_IMAGE} alt={nombreSEO != null ? nombreSEO : nombre} width="300" height="300" />
                </div>

                <div className="carruselContenido">
                    <div className="descrip">
                        <p >{nombre}</p>
                    </div>

                    <div className="info">
                        <div className="infoDesc">
                            {/* precio anterior */}
                            {descuento !== 0 && precioLista !== precio
                                ? <CurrencyFormat
                                    value={precio} displayType={'text'}
                                    thousandSeparator={true} prefix={'$'}
                                    renderText={value => <span className="precioant">{value}</span>}
                                />
                                : null
                            }
                            {/* precio con descuento */}
                            <CurrencyFormat
                                value={precioLista} displayType={'text'}
                                thousandSeparator={true} prefix={'$'}
                                renderText={value => <span className="preciodesc">{value} </span>}
                            />
                            {/* descuento */}
                            {descuento <= HIDDEN_DISCOUNT
                                ? null
                                : <div className="descText"><span>- {descuento}% </span></div>
                            }
                        </div>

                        <div className="contentPromo">
                            <div className="envioCard">
                                {shipping_price
                                    ? shipping_price === "1"
                                        ? <span>Envío gratis</span>
                                        : null
                                    : precioLista >= ENVIO_GRATIS
                                        ? <span>Envío gratis</span>
                                        : null
                                }
                            </div>
                        </div>

                    </div>


                </div>
            </Link>
        </article>
    )
}

export default function BusquedaProducto(props) {
    const params = props.param
    const [searchResults, setSearchResults] = useState([])
    const [searchParams, setSearchParams] = useState({
        busqueda: null,
        marca: null,
        precioMenor: null,
        precioMayor: null,
        categoria: null,
        page: null
    })

    const [loading, setLoading] = useState(true)
    const [flagInicial, setFlagInicial] = useState(false)
    const [flagFinal, setFlagFinal] = useState(false)
    const [upperPageBound, setUpperPageBound] = useState(3)
    const [lowerPageBound, setLowerPageBound] = useState(0)
    const [pageBound, setPageBound] = useState(3)

    const incrementPage = () => {
        window.scrollTo(0, 0)
        if ((searchParams.page + 1) > upperPageBound) {
            setUpperPageBound( upperPageBound + pageBound );
            setLowerPageBound(lowerPageBound + pageBound );
        }
        let listid = searchParams.page + 1;
        setSearchParams({
            ...searchParams,
            page: listid
        });
    }

    const decrementPage = () => {
        window.scrollTo(0, 0)
        if ((searchParams.page - 1) % pageBound === 0) {
            setUpperPageBound( upperPageBound - pageBound );
            setLowerPageBound( lowerPageBound - pageBound );
        }
        let listid = searchParams.page - 1;
        setSearchParams({
            ...searchParams,
            page: listid
        });
    }

    const ultimatePage = () => {
        window.scrollTo(0, 0)
        setSearchParams({
            ...searchParams,
            page: (Math.ceil(searchResults.searchResult.GSP.RES.M / PRODUCTS_PER_PAGINATION))
        });
        setFlagInicial(false)
        setFlagFinal(true)
    }

    const initialPage = () => {
        window.scrollTo(0, 0)
        setSearchParams({
            ...searchParams,
            page: 1
        });

        setFlagInicial(true)
        setFlagFinal(false)
    }

    const updateParams = ({ params }) => {
        const urlParams = params.split('/')
        console.log("url params :", urlParams)
        urlParams.shift()
        urlParams.shift()

        var busqueda = urlParams[0].replace('q=', '')
        var marca = null
        var precioMenor = null
        var precioMayor = null
        var categoria = null
        var page = urlParams[urlParams.length - 1]

        for (let i = 0; i <= (urlParams.length - 1); i++) {

            if (urlParams[i].indexOf('marca=') >= 0) {
                marca = urlParams[i].replace('marca=', '')
            }
            if (urlParams[i].indexOf('precio-menor=') >= 0) {
                precioMenor = urlParams[i].replace('precio-menor=', '')
            }
            if (urlParams[i].indexOf('precio-mayor=') >= 0) {
                precioMayor = urlParams[i].replace('precio-mayor=', '')
            }
            if (urlParams[i].indexOf('categoria=') >= 0) {
                categoria = urlParams[i].replace('categoria=', '')
            }
        }

        if (urlParams.length === 1) {
            page = 1
        }

        setSearchParams({
            busqueda: busqueda,
            marca: marca,
            precioMenor: precioMenor,
            precioMayor: precioMayor,
            categoria: categoria,
            page: page
        })

        // console.log("--------> marca: ", marca, " precio Mayor: ", precioMayor, precioMenor, " categoria: ", categoria, " pagina: ", page, " busqueda: ", busqueda)
    }

    const handleClick = (event) => {
        let listid = Number(event.target.id);
        setSearchParams({
            ...searchParams,
            page: listid
        });

        $("ul li a.activarPaginacion").removeClass('activarPaginacion');
        $('ul li#' + listid + ' a ').addClass('activarPaginacion');
    }

    const btnIncrementClick = () => {
        setUpperPageBound( upperPageBound + pageBound );
        setLowerPageBound( lowerPageBound + pageBound );
        let listid = upperPageBound + 1;

        setSearchParams({
            ...searchParams,
            page: listid

        });
    }

    const btnDecrementClick = () => {
        setUpperPageBound(upperPageBound - pageBound );
        setLowerPageBound(lowerPageBound - pageBound );
        let listid = upperPageBound - pageBound;

        setSearchParams({
            ...searchParams,
            page: listid
        });
    }

    useEffect(() => {
        $("ul li a.activarPaginacion").removeClass('activarPaginacion');
        $('ul li#' + Number(searchParams.page) + ' a ').addClass('activarPaginacion');
    });

    useEffect(() => { updateParams({ params }) }, [params])

    useEffect(() => {
        if (!searchParams.busqueda) {
            return
        } else {
            getSearch({ searchText: searchParams.busqueda, brand: searchParams.marca, lowerPrice: searchParams.precioMenor, higherPrice: searchParams.precioMayor, id_Category: searchParams.categoria, order: '', pagination: searchParams.page })
                .then(setSearchResults)
        }
    }, [searchParams])

    useEffect(() => { setLoading(false) }, [searchResults])

    const url_Per_Pagination = params.replace('/' + searchParams.page, '')

    const pageNumbers = [];
    if (searchResults.searchResult) {
        for (let i = 1; i <= Math.ceil(searchResults.searchResult.GSP.RES.M / PRODUCTS_PER_PAGINATION); i++) {
            pageNumbers.push(i);
        }
    }

    const renderPageNumbers = pageNumbers.map(number => {
        if (number === 1 && Number(searchParams.page) === 1) {
            return (
                <li key={number} id={number}>
                    <Link className='activarPaginacion' to={`${url_Per_Pagination}/${number}`} id={number} onClick={handleClick}>{number}</Link>
                </li>
            )
        }
        else if ((number < upperPageBound + 1) && number > lowerPageBound) {
            return (
                <li key={number} id={number}>
                    <Link to={`${url_Per_Pagination}/${number}`} onClick={handleClick}>{number}</Link>
                </li>
            )
        }
    });

    // botones ... incrementar
    let pageIncrementBtn = null;
    if (pageNumbers.length > upperPageBound) {
        pageIncrementBtn = <li className=''><Link to={`${url_Per_Pagination}/${Number(searchParams.page) + 1}`} onClick={btnIncrementClick}> &hellip; </Link></li>
    }
    // botones ... decrementar
    let pageDecrementBtn = null;
    if (lowerPageBound >= 1) {
        pageDecrementBtn = <li className=''><Link to={`${url_Per_Pagination}/${Number(searchParams.page) - 1}`} onClick={btnDecrementClick}> &hellip; </Link></li>
    }

    return (
        <div className="sec__conent">
            <aside className="conent__filtro"> filtros</aside>

            <div className="conent__productos">
                <div className="container">
                    <div className="titleProductoCategoria">
                        <h2>Productos</h2>
                        <div className="sec">
                            {searchResults.searchResult
                                ? <div className="resultadoBusqueda" id="resultadoBusqueda"> {Math.ceil(searchResults.searchResult.GSP.RES.M)} productos encontrados.</div>
                                : null
                            }

                            <div className="vistas">
                                <p>Vistas</p>
                                <a href="!#" className="listado"> </a>
                                <a href="!#" className="mosaico"> </a>
                            </div>

                            <div className="ordenar__prod">
                                <span className="select option2">
                                    <select id="ordenar" name="orden">
                                        <option>Menor Precio</option>
                                        <option>Mayor Precio</option>
                                    </select>
                                </span>
                            </div>

                            <div className="filtroMobile">
                                <a href="!#" className="anclaClategoria"> </a>
                                <a href="!#" className="cerrarCajaFiltros"> </a>
                                <a href="!#" className="open-filtros"> <i></i> </a>
                            </div>
                        </div>
                    </div>

                    {searchResults.searchResult &&
                        <>
                            <div className="contentProductsCategory">
                                <div className="rowProductos" id="productos">
                                    {/* {Number(searchResults.searchResult.GSP.RES.M) === 0 &&
                        <ErrorSearchServices searchQ={searchResults.searchResult.GSP.Q} suggestion={searchResults.searchResult.GSP.Spelling ? searchResults.searchResult.GSP.Spelling.Suggestion.V : null} />
                    } */}
                                    {Number(searchResults.searchResult.GSP.RES.M) !== 0 &&
                                        <>
                                            {/* <BarFilter results={searchResults.searchResult.GSP.RES.M} /> */}
                                            {
                                                searchResults.searchResult.GSP.RES.R.map(({ MT }) =>
                                                    <Fragment key={MT[MT.findIndex(item => item.N === 'id')].V}>
                                                        <Producto
                                                            key={MT[MT.findIndex(item => item.N === 'id')].V}
                                                            id={MT[MT.findIndex(item => item.N === 'id')] ? MT[MT.findIndex(item => item.N === 'id')].V : null}
                                                            nombre={MT[MT.findIndex(item => item.N === 'title')] ? MT[MT.findIndex(item => item.N === 'title')].V : null}
                                                            nombreSEO={MT[MT.findIndex(item => item.N === 'title_seo')] ? MT[MT.findIndex(item => item.N === 'title_seo')].V : null}
                                                            precio={MT[MT.findIndex(item => item.N === 'price')] ? MT[MT.findIndex(item => item.N === 'price')].V : null}
                                                            precioLista={MT[MT.findIndex(item => item.N === 'sale_price')] ? MT[MT.findIndex(item => item.N === 'sale_price')].V : null}
                                                            foto={MT[MT.findIndex(item => item.N === 'link_thumbnail')] ? MT[MT.findIndex(item => item.N === 'link_thumbnail')].V : null}
                                                            descuento={MT[MT.findIndex(item => item.N === 'discount')] ? MT[MT.findIndex(item => item.N === 'discount')].V : null}
                                                            store={MT[MT.findIndex(item => item.N === 'store')] ? MT[MT.findIndex(item => item.N === 'store')].V : null}
                                                            review={MT[MT.findIndex(item => item.N === 'review')] ? MT[MT.findIndex(item => item.N === 'review')].V : null}
                                                            shipping_price={MT[MT.findIndex(item => item.N === 'shipping_price')] ? MT[MT.findIndex(item => item.N === 'shipping_price')].V : null}
                                                        />
                                                    </Fragment>
                                                )
                                            }
                                        </>
                                    }
                                </div>

                                {/* {console.log("pagina actual: ", Number(searchParams.page))} */}
                                {/* {console.log("cantidad de paginas:", Math.ceil(searchResults.searchResult.GSP.RES.M / PRODUCTS_PER_PAGINATION))} */}

                                <div className="paginador">
                                    <ul>
                                        {/* visualizar back */}
                                        {Number(searchParams.page) > 1
                                            ? <Fragment>
                                                <li> <Link to={`${url_Per_Pagination}/${Number(searchParams.page) - 1}`} onClick={decrementPage} className="back"> </Link> </li>
                                                <li> <Link to={`${url_Per_Pagination}/${1}`} onClick={initialPage} className={flagInicial === true ? "backback activarPaginacion" : "backback"}> </Link> </li>
                                            </Fragment>
                                            : null
                                        }

                                        {pageDecrementBtn}
                                        {renderPageNumbers}
                                        {pageIncrementBtn}

                                        {/* visualizar next e ir ultima pagina */}
                                        {Number(searchParams.page) !== Math.ceil(searchResults.searchResult.GSP.RES.M / PRODUCTS_PER_PAGINATION)
                                            ? <Fragment>
                                                <li> <Link to={`${url_Per_Pagination}/${Math.ceil(searchResults.searchResult.GSP.RES.M / PRODUCTS_PER_PAGINATION)}`}
                                                    onClick={ultimatePage} className={flagFinal ? "nextnext activarPaginacion" : "nextnext"}> </Link>
                                                </li>
                                                <li> <Link to={`${url_Per_Pagination}/${Number(searchParams.page) + 1}`}
                                                    onClick={incrementPage} className="next"> </Link>
                                                </li>
                                            </Fragment>
                                            : null
                                        }
                                    </ul>
                                </div>
                            </div>
                        </>
                    }
                </div>
            </div>
        </div >
    );
}