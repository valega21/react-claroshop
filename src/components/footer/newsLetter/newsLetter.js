import React from 'react';
import './newsLetter.sass';


const Newsletter = props =>{

   const{
        title = '¡Regístrate a nuestro Newsletter!',
        errorMessage = "Por favor ingresa un correo electrónico válido",
        submitBoletin,
        handleChange,
        emailEmpty
    } = props

    return(
        <div className="foterNewsletter">
            <div className="newsletterBlock">
                <div className="nlMessage">
                    <h5>{title}</h5>
                </div>

                <div id="messageBoletin" className={emailEmpty ? 'ventana_emergente erroresEmergen' : 'ventana_emergente erroresEmergen active'} >{errorMessage}</div>
                
                <form onSubmit={submitBoletin} className="nlform">
                    <input onChange={handleChange} type="email" name="newsletter" placeholder="Tu correo electrónico" className="form-control" />
                    <button name="subscription" onClick={handleChange} value="1" className="btn-nh">Para Mujeres</button>
                    <button name="subscription" onClick={handleChange} value="2" className="btn-nh">Para Hombres</button>
                </form>
            </div>
        </div>
    )
}

export default Newsletter;