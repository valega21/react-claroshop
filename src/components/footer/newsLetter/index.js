import React, { Component } from 'react';
import NewsLetter from './newsLetter';
import sendNewsletter from '../../../redux/actions/sendNewsletter';

export default class FooterNeswletter extends Component {

    constructor(props) {
        super(props)
        this.state = {
            newsletter: '',
            subscription: '',
            emailEmpty: true
        }
    }

    validaMail = (stats) => {
        this.setState({ emailEmpty: stats })
    }

    sendDataMail = () => {
        var dataNewsL = {
            correo: this.state.newsletter,
            genero: this.state.subscription
        }
        sendNewsletter(dataNewsL)
    }

    handleChange = (e) => {
        const { name, value } = e.target
        this.setState({
            [name]: value
        })
        this.validaMail(true)
    }

    submitBoletin = (e) => {
        e.preventDefault();
        !this.state.newsletter ?
            this.validaMail(false)
            :
            this.sendDataMail()
    }

    render() {
        return (
            <NewsLetter
                submitBoletin={this.submitBoletin}
                handleChange={this.handleChange}
                emailEmpty={this.state.emailEmpty}
            />
        )
    }
}