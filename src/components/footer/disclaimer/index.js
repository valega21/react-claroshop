import React, { Component } from 'react';
import Disclaimer from './disclaimer';

export default class DisclaimerNotice extends Component {

    constructor(props) {
        super(props)
        this.state = {}
    }

    render(props) {

        const {
            descripDisclamer
        } = this.props

        return (
            <Disclaimer descripDisclamer={descripDisclamer} />
        )
    }
}

DisclaimerNotice.defaultProps = {
    descripDisclamer: '*La promoción aplica sólo a productos participantes. Precios y ofertas sujetos a cambios sin previo aviso. Hasta agotar existencias.'
}