import React from 'react';
import './disclaimer.sass';

const Disclaimer = props => {
    const {
        descripDisclamer
    } = props

    return (
        <div className="disclaimerNotice">
            <p>{descripDisclamer}</p>
        </div>
    )
}

export default Disclaimer;