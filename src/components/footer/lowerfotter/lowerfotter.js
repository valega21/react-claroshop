import React from 'react';
import { Link } from 'react-router-dom';
import './lowerfotter.sass';

const Lower = props =>{

    const {
        terminos,
        politicas
    } = props
    
    return(
        <div className="lowerfooter">
            <div className="legales">
                <Link to={terminos.url} title={terminos.title}>{terminos.title}</Link>
                <span>|</span>
                <Link to={politicas.url} title={politicas.title}>{politicas.title}</Link>
            </div>
        </div>
    )
}

export default Lower;