import React, { Component } from 'react';
import Lower from './lowerfotter';

export default class LowerFooter extends Component {

    render() {
        const {
            terminos,
            politicas
        } = this.props

        return (
            <Lower terminos={terminos} politicas={politicas} />
        )
    }
}

LowerFooter.defaultProps = {
    terminos: {
        title: 'Términos y condiciones',
        url: '/terminos-y-condiciones/'
    },
    politicas: {
        title: 'Políticas de privacidad',
        url: '/aviso-privacidad/'
    }
}