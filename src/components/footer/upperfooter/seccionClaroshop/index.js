import React, { Component } from 'react';
import SeccionClaro from './seccionClaroshop';
import './seccionClaroshop.sass';

export default class BoxClaroshop extends Component {
    constructor(props) {
        super(props);
        this.state = {
            menuView: false
        }
    }

    toggleClass = () => {
        this.setState({ menuView: !this.state.menuView })
    }

    render() {
        return (
            <SeccionClaro
                toggleClass={this.toggleClass}
                menuView={this.state.menuView}
            />
        )
    }
}
