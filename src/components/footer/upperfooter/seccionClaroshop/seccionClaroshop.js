import React from 'react';
import { Link } from 'react-router-dom';

const SeccionClaro = props => {
    const {
        toggleClass,
        menuView
    } = props

    return (
        <div className="boxClaroShop">
            <h4 onClick={toggleClass} className={menuView ? 'active' : null}>Claro Shop</h4>
            <ul>
                <li><Link to="/aviso-privacidad/">Aviso de privacidad</Link></li>
                <li><Link to="/terminos-y-condiciones/">Términos y condiciones</Link></li>
            </ul>
        </div>
    )
}

export default SeccionClaro;