import React from 'react';
import { RESOURCES } from '../../../../constants/endPoints';
import './seccionRedes.sass';

const SeccionRedes = () => {
    return (
        <div className="boxRedes">
            <div className="redesLogos">
                <span className="sigTitle">Síguenos:</span>
                <ul>
                    <li>
                        <a href="https://www.facebook.com/claroshopmx/?ref=hl" aria-label="icon-face">
                            <img src={RESOURCES + "img/newHome/icon-face.svg"} alt="icon-face" />
                        </a>
                    </li>
                    <li>
                        <a href="https://twitter.com/claroshop_com/" aria-label="icon-twitter">
                            <img src={RESOURCES + "img/newHome/icon-twitter.svg"} alt="icon-twitter" />
                        </a>
                    </li>
                    <li>
                        <a href="https://www.youtube.com/channel/UCNEr65Oyg6W6fn9Dm_4pcLQ">
                            <img src={RESOURCES + "img/newHome/icon-youtube.svg"} alt="icon-youtube" />
                        </a>
                    </li>
                    <li>
                        <a href="https://instagram.com/claroshop_com/">
                            <img src={RESOURCES + "img/newHome/icon-instagram.svg"} alt="icon-instagram" />
                        </a>
                    </li>
                </ul>
            </div>

            <div className="downloadApp">
                <span className="sigTitle">Descarga la app en:</span>
                <ul>
                    <li>
                        <a href="https://play.google.com/store/apps/details?id=com.americamovil.claroshop">
                            <img src={RESOURCES + "img/newHome/google_play.svg"} alt="google_play" />
                        </a>
                    </li>
                    <li>
                        <a href="https://itunes.apple.com/mx/app/claro-shop/id1057314145?mt=8">
                            <img src={RESOURCES + "img/newHome/app_store.svg"} alt="app_store" />
                        </a>
                    </li>
                </ul>
            </div>

            <div className="certificacionApp">
                <span className="sigTitle">Certificación en:</span>
                <ul>
                    <li>
                        <a href="https://www.sellosdeconfianza.org.mx/MuestraCertificado.php?NUMERO_SERIE=MD_v100" target="_blank" rel="follow noopener noreferrer">
                            <img src={RESOURCES + "medios-plazavip/swift/v1/claroshop/claroshop/landings/cintillo/Sello_Datos.jpg"} alt="Amipci" />
                        </a>
                    </li>
                    <li>
                        <img src={RESOURCES + "css/img/paypalCompraSegura.jpg"} alt="Paypal Compra Segura" />
                    </li>
                </ul>
            </div>
        </div>
    )
}

export default SeccionRedes;