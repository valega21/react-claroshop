import React from 'react';
import { Link } from 'react-router-dom';
import './seccionServicioCliente.sass';

const SeccionServicioCliente = props => {

    const {
        toggleClass,
        menuView
    } = props

    return (
        <div className="boxServicioCliente">
            <h4 onClick={toggleClass} className={menuView ? 'active' : null}>Servicio al Cliente</h4>
            <ul>
                <li><a href="http://facturacionelectronica.claroshop.com/">Facturación electrónica</a></li>
                <li><Link to="/claroPromo/">Promociones diversas vigentes</Link></li>
                <li><Link to="/formas-pago/">Formas de pago</Link></li>
                <li><Link to="/politica-devoluciones/">Cambios y devoluciones</Link></li>
                <li><Link to="/l/faqs/">Preguntas frecuentes</Link></li>
            </ul>
        </div>
    )
}

export default SeccionServicioCliente;