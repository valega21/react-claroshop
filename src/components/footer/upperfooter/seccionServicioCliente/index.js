import React, { Component } from 'react';
import SeccionServicioCliente from './seccionServicioCliente';

export default class BoxServiceClient extends Component {

    constructor(props) {
        super(props)
        this.state = {
            menuView: false
        }
    }

    toggleClass = () => {
        this.setState({ menuView: !this.state.menuView })
    }

    render() {
        return (
            <SeccionServicioCliente
                toggleClass={this.toggleClass}
                menuView={this.state.menuView}
            />
        )
    }
}