import React from 'react';
import BoxClaroshop from '../../../components/footer/upperfooter/seccionClaroshop';
import BoxServicioCliente from '../../../components/footer/upperfooter/seccionServicioCliente';
import BoxRedes from '../../../components/footer/upperfooter/seccionRedes';
import BoxContacto from '../../../components/footer/upperfooter/seccionContacto';
import './upperfooter.sass';

const UpperFooter = () => {
    return (
        <div className="upperfooter">
            <BoxClaroshop />
            <BoxServicioCliente />
            <BoxRedes />
            <BoxContacto />
        </div>
    )
}
export default UpperFooter;