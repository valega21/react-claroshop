import React, { Component } from 'react';
import SeccionContacto from './seccionContacto';

export default class BoxContacto extends Component {

    constructor(props) {
        super(props)
        this.state = {
            menuView: false
        }
    }

    toggleClass = () => {
        this.setState({ menuView: !this.state.menuView })
    }

    render() {
        return (
            <SeccionContacto
                toggleClass={this.toggleClass}
                menuView={this.state.menuView}
            />
        )
    }
}