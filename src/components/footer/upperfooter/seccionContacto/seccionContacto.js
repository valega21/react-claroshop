import React from 'react';
import './seccionContacto.sass';

const SeccionContacto = props => {
    const {
        toggleClass,
        menuView
    } = props

    return (
        <div className="boxContacto">
            <h4 onClick={toggleClass} className={menuView ? 'active' : null} >Contáctanos</h4>
            <ul>
                <li>República Mexicana</li>
                <li><a href="tel:018008906566">01800-890-6566</a></li>
                <li>CDMX y área metropolitana</li>
                <li><a href="tel:12030502">1203-0502</a></li>
                <li>Correo: <a href="mailto: clientes@claroshop.com">clientes@claroshop.com</a></li>
            </ul>
        </div>
    )
}

export default SeccionContacto;