import React, { Component } from 'react';
import MPago from './metodospago';
import LowerFotter from './lowerfotter';
import Disclaimer from './disclaimer/disclaimer';
import FooterNeswletter from './newsLetter';
import UpperFooter from './upperfooter';

export default class Footer extends Component {

    render() {
        return (
            <div className="nhfooter">
                <FooterNeswletter />
                <UpperFooter />
                <MPago />
                <Disclaimer />
                <LowerFotter />
            </div>
        )
    }
}
