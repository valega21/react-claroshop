import React from 'react';
import './metodospago.sass';

import { RESOURCES } from '../../../constants/endPoints';

const MetodosPago = props => {
    return (
        <div className="nhmetodospago">
            <span>Formas de pago</span>
            <div className="mplogos">
                <img src={RESOURCES + "img/newHome/icon-telmex-gray.svg"} alt="icon-telmex" />
                <img src={RESOURCES + "img/newHome/icon-sears-gray.svg"} alt="icon-sears" />
                <img src={RESOURCES + "img/newHome/icon-sanborns-gray.svg"} alt="icon-sanborns" />
                <img src={RESOURCES + "img/newHome/icon-inbursa-gray.svg"} alt="icon-inbursa" />
                <img src={RESOURCES + "img/newHome/icon-visa-gray.svg"} alt="icon-visa-gray" />
                <img src={RESOURCES + "img/newHome/icon-mastercard-gray.svg"} alt="icon-mastercard" />
                <img src={RESOURCES + "img/newHome/icon-amex-gray.svg"} alt="icon-amex" />
                <img src={RESOURCES + "img/newHome/icon-paypal-gray.svg"} alt="icon-paypal" />
                <img src={RESOURCES + "img/newHome/icon-7eleven-gray.svg"} alt="icon-7eleven" />
                <img src={RESOURCES + "img/newHome/icon-oxxo-gray.svg"} alt="icon-oxxo" />
                <img src={RESOURCES + "img/newHome/icon-bbva-gray.svg"} alt="icon-bbva" />
                <img src={RESOURCES + "img/newHome/icon-banamex-gray.svg"} alt="icon-citybanamex" />
                <img src={RESOURCES + "img/newHome/icon-santander-gray.svg"} alt="icon-santander" />
            </div>
        </div>
    )
}

export default MetodosPago;