import React from 'react';
import './carrouselProductos.sass';
import { Link } from 'react-router-dom';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";

// componente para imagenes del slider
const Sli = ({ url, imagen, alt, tituloProducto, precioAnterior, PrecioActual, descuento, favorito, onClickFavorito }) => {
    return (
        <article className="cardProduct">

            <div onClick={onClickFavorito} className={favorito === true ? "wishlistButton active" : "wishlistButton"} >
            </div>

            <Link to={'/producto/' + url}>
                <div className="contImgProd">
                    <img src={imagen} alt={alt} className="lazyload" />
                </div>
                <div className="carruselContenido">
                    <div className="descrip">
                        <p>{tituloProducto}</p>
                    </div>
                    <div className="info">
                        <div className="infoDesc">
                            {precioAnterior !== PrecioActual
                                ? <span className="precioant">${precioAnterior}</span>
                                : <span className="precioant"></span>
                            }
                            <span className="preciodesc">${PrecioActual}
                                {precioAnterior !== PrecioActual
                                    ? <div className="descText">
                                        <span>-{descuento}%</span>
                                    </div>
                                    : null
                                }
                            </span>
                        </div>
                        {PrecioActual >= 499
                            ? <div className="contentPromo">
                                <div className="envioCard">
                                    <span>Envío Gratis</span>
                                </div>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </Link>
        </article >
    )
}

const Carrousel = ({ titulo, productos }) => {
    var settings = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 5,
        slidesToScroll: 1
    };

    return (
        <section className="carruselesNewH">
            <h2>
                {titulo}
                <Link className="link" to="">Ver más</Link>
            </h2>
            <div className="rowProductos">
                <Slider {...settings} id="slidedia">
                    {productos.map(item => (
                        <Sli url={item.id}
                            imagen={item.foto} alt={item.nombre} key={item.id}
                            tituloProducto={item.nombre} precioAnterior={item.precioLista}
                            PrecioActual={item.precio} descuento={item.descuento} favorito=""
                        />
                    ))}
                </Slider>
            </div>
        </section>
    )
}

export default Carrousel;