import React, { Component } from 'react';
import Carrousel from './carrouselProductos';

class CarrouselProductos extends Component {

    constructor(props) {
        super(props);
        this.state = {
            favorito: false
        }
    }

    onClickFavorito = () => {
        this.setState({ favorito: !this.state.favorito })
    }

    render() {
        return (
            <Carrousel titulo={this.props.titulo} productos={this.props.productos}
                favorito={this.state.favorito} onClickFavorito={this.onClickFavorito}
            />
        )
    }
}

export default CarrouselProductos;