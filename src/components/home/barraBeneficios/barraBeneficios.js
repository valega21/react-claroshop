import React from 'react';
import { Link } from 'react-router-dom';
import { PRODUCTION_RES } from '../../../constants/endPoints';
import './barraBeneficios.sass';

const BarraBeneficios = () => {
    const ruta = "medios-plazavip/swift/v1/claroshop/claroshop/landings/cintillo/";

    return (
        <section className="sectionBeneficios">
            <div className="container">
                <Link to="/l/compra-con-recibo-Telmex/" className="cardBeneficios bTelmex">
                    <p>Compra con Recibo Telmex</p>
                    <img src={PRODUCTION_RES + ruta + 'telmexLogo.png'} alt="Compra con Recibo" />
                </Link>

                <div className="cardBeneficios bOriginal">
                    <p>Productos 100% Originales</p>
                    <img src={PRODUCTION_RES + ruta + 'gifOriginal.png'} alt="Productos 100% Originales" />
                </div>

                <a className="cardBeneficios bSegura" href="https://sellosdeconfianza.org.mx/MuestraCertificado.php?NUMERO_SERIE=MD_v100" target="_blank" rel="noopener noreferrer">
                    <p>Compra Segura</p>
                    <img src={PRODUCTION_RES + ruta + 'Sello_Datos.jpg'} alt="Compra Segura" />
                </a>

                <div className="cardBeneficios bGratis">
                    <p>
                        Envío gratis a todo México
                        <span>a partir de $499</span>
                    </p>
                    <img alt="Envío Gratis" src={PRODUCTION_RES + 'medios-plazavip/swift/v1/claroshop/assets/img/Carrito_Envio1.svg'} />
                </div>
            </div>
        </section>
    )
}

export default BarraBeneficios;