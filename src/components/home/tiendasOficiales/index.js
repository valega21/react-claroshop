import React, { Component } from 'react';
import Tiendas from './tiendasOficiales';
import { Link } from 'react-router-dom';
import './tiendasOficiales.sass';

class TiendasOficialesComponent extends Component {

    render() {
        const { dataHome } = this.props;
        const arregloCarrusel = Object.values(Object.assign({}, dataHome))
        const tiendas = []

        arregloCarrusel.map((ar) => {
            var carru = Object.entries(ar)
            carru.map((car, j) => {
                return (car[0] === "tiendas"
                    ?
                    car[1].map((uno, i) => {
                        tiendas.push({ nombre: uno.nombre, id: uno.id, image: uno.image })
                    })
                    : null
                )
            })
        })

        // console.log("tiendas: ", tiendas)
        const tienda = tiendas.map((uno, i) => {
            return (i <= 3
                ? <Tiendas key={i} nombre={uno.nombre} imagen={uno.image} id={uno.id} />
                : null
            )
        })

        return (
            <section className="carruselesNewH tiendasOficiales">
                <h2>
                    Tiendas Oficiales
                    <Link to="/tiendas" className="link">Ver todas las tiendas</Link>
                </h2>
                <div className="contentTiendasNew">
                    <div className="marcasTiendas">
                        {tienda}
                    </div>
                    <div id="tiendasDestacadas" className="slideTiendaD slick-initialized slick-slider">

                    </div>
                </div>
            </section>
        );
    }
}

export default TiendasOficialesComponent;