import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import './tiendasOficiales.sass';

const Tiendas = ({ imagen, nombre, id }) => {
    return (
        <Fragment>
            <Link to={"Tienda/" + id + "/" + nombre}>
                <img src={imagen} alt={nombre} />
            </Link>

        </Fragment>
    );
}

export default Tiendas;