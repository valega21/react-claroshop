import React, { Component, Fragment } from 'react';
import BarraBeneficios from './barraBeneficios';
import WhatsappAyuda from './whatsapp';
import SliderPrincipalComponent from './sliderPrincipal';
import CarrouselProductos from './carrouselProductos';
import { connect } from 'react-redux';
import getDataHomeProd from '../../redux/actions/getHome';
import TiendasOficialesComponent from './tiendasOficiales';

class HomeComponent extends Component {

    componentDidMount() {
        this.props.getDataHomeProd();
    }

    render() {
        const { dataHome } = this.props;
        const arregloCarrusel = Object.values(Object.assign({}, dataHome))
        const interno = []
        console.log("data: ", dataHome)

        arregloCarrusel.map((ar) => {
            var carru = Object.entries(ar)
            carru.map((car, j) => {
                return (car[0] === "carruseles"
                    ?
                    car[1].map((uno, i) => {
                        interno.push({ nombre: uno.nombre, producto: uno.productos, indice: i })
                    })
                    : null
                )
            })
        })

        const carrusel = interno.map((uno, i) => {
            return (< CarrouselProductos titulo={uno.nombre} productos={uno.producto} key={i} />)
        })


        return (
            <Fragment>
                <SliderPrincipalComponent />
                <BarraBeneficios />
                {carrusel}
                <TiendasOficialesComponent  dataHome={dataHome}/>
                <WhatsappAyuda />
            </Fragment>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataHome: state.dataHomeProduct
    }
}

const mapDispatchToProps = {
    getDataHomeProd
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeComponent);