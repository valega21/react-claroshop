import React from 'react';

const Whatsapp = () => {
    return (
        <a href="https://api.whatsapp.com/send?phone=525518110000" target="_blank" rel="noopener noreferrer">
            <div className="flex-column-container float">
                <i className="fa fa-whatsapp"></i>
                <p className="text">Ayuda</p>
            </div>
        </a>
    )
}

export default Whatsapp;