import React from 'react';
import './seccionMain.sass';
import { Link } from 'react-router-dom';

const SeccionTop = ({ ruta, imagen, alt }) => {
    return (
        <div className="bannersTop">
            <div className="bannerOne">
                <Link to={ruta}>
                    <img src={imagen} alt={"imagen del banner a " + alt} />
                </Link>
            </div>
        </div>
    )
}

export default SeccionTop;