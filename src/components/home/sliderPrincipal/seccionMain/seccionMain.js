import React from 'react';
import './seccionMain.sass';
import SeccionTop from './seccionTop';
import MagazineTop from './magazinetop';
import { PRODUCTION_RES } from '../../../../constants/endPoints'

const SeccionMain = ({ ruta, imagen, alt }) => {

    return (
        <div className="mainSection">
            <MagazineTop />
            <SeccionTop ruta={ruta} imagen={PRODUCTION_RES + imagen} alt={alt} />
        </div>
    )
}

export default SeccionMain;

SeccionMain.defaultProps = {
    ruta: 'https://www.claroshop.com/producto/973264/pantalla-tcl-50quot-smart-tv-roku-tv-4k-uhd-50s42545mx?utm_source=directo&utm_medium=optimize&utm_campaign=producto-bomba',
    imagen: 'medios-plazavip/mkt/5e855b411985e_infojpg.jpg',
    alt: 'descripcion de la imagen'
}