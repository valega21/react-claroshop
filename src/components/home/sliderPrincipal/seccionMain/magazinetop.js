import React, { Fragment } from 'react';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import './seccionMain.sass';
import { Link } from 'react-router-dom';

const arreglo = [
    {
        url: '#',
        imagen: 'https://resources.claroshop.com/medios-plazavip/mkt/5ed5c3c48a3b4_1seextienden-leatherjpg.jpg',
        alt: ''
    },
    {
        url: '#',
        imagen: 'https://resources.claroshop.com/medios-plazavip/mkt/5ed5d92e11687_leater_mueblesjpg.jpg',
        alt: ''

    },
    {
        url: '#',
        imagen: 'https://resources.claroshop.com/medios-plazavip/mkt/5ed5de4d11861_lo-mas-vendido-leaterjpg.jpg',
        alt: ''
    }
]

// componente para imagenes del slider
const Sli = ({ url, imagen, alt, indice }) => {
    return (
        <Link to={url}>
            <img src={imagen} alt={'imagen del banner a ' + alt} className={indice > 0 ? 'borde' : ''} />
        </Link>
    )
}

// componente para mostrar imagenes en responsive
const Movil = () => {
    return (
        <div className="banner">
            {arreglo.map((item, i) => {
                return <Sli url={item.url} imagen={item.imagen} alt={item.alt} indice={i} key={i} />
            })}
        </div>
    )
}

const MagazineTop = () => {
    var settings = {
        dots: false,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear',
        autoplay: true,
        autoplaySpeed: 2000,
    };

    return (
        <Fragment>
            <div className="magazineTop">
                <Slider {...settings} id="SliderHomePrincipal">
                    {arreglo.map((item, i) => {
                        return (<Sli url={item.url} imagen={item.imagen} alt={item.alt} key={item.ruta + i} />)
                    })}
                </Slider>
            </div>

            <Movil />
        </Fragment>
    )
}

export default MagazineTop;