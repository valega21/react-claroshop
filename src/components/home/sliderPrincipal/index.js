import React, { Component } from 'react';
import ProductosHotComponent from './productosHot';
import SeccionMainComponent from './seccionMain';

class SliderPrincipalComponent extends Component {
    render() {
        return (
            <section className="sec__slider">
                <SeccionMainComponent />
                <ProductosHotComponent />
            </section>
        )
    }
}

export default SliderPrincipalComponent;