import React, { Component } from 'react';
import ProductosHot from './productosHot';
import { PRODUCTION_RES, PRODUCTION } from '../../../../constants/endPoints';
import { connect } from 'react-redux';
import productoHotData from '../../../../redux/actions/getProductoHot';

class ProductosHotComponent extends Component {

    componentDidMount() {
        this.props.productoHotData();
    }

    render() {
        const { productoHot } = this.props.productoHot
        const { images } = productoHot

        var obj = Object.assign({}, images);
        var cero = Object.assign({}, obj[0])
        var img = cero.link

        return (
            <ProductosHot
                ruta={'/producto/' + productoHot.id} imagen={img}
                nombre={productoHot.title} precioAntes={productoHot.price}
                precioAhora={productoHot.sale_price} descuento={productoHot.discount}
            />
        )
    }
}

const mapStateToProps = (state) => {
    return {
        productoHot: state.productoHot
    }
}

const mapDispatchToProps = {
    productoHotData
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductosHotComponent);

//ruta: 'https://www.claroshop.com/producto/973264/pantalla-tcl-50quot-smart-tv-roku-tv-4k-uhd-50s42545mx?utm_source=directo&utm_medium=optimize&utm_campaign=producto-bomba',
