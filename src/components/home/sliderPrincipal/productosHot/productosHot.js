import React from 'react';
import './productosHot.sass';
import { Link } from 'react-router-dom';

const ProductosHot = ({ ruta, imagen, nombre, precioAntes, precioAhora }) => {
    return (
        <section id="productoHot">
            <Link to={ruta}>
                <div className="producto">
                    <div className="producto-img">
                        <img src={imagen} alt="producto" />
                    </div>

                    <div className="producto-caracteristicas">
                        <div className="producto-nombre">
                            <p>{nombre}</p>
                        </div>

                        <div className="producto-precio">
                            <p className="precio-antes">${precioAntes}</p>
                            <p className="precio-ahora">${precioAhora}MXN</p>
                        </div>

                        <div className="producto-boton">
                            <p>Comprar</p>
                        </div>
                    </div>
                </div>
            </Link>
        </section>
    )
}

export default ProductosHot;