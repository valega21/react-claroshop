import React, { Component } from 'react';
import Cintillo from "./cintilloMSI";
import './cintilloMSI.sass';
import {PATH_PORTAL} from '../../../constants/endPoints';

class CintilloMSIComponent extends Component {
    render() {
        const {
            imaDesktop, imaResponsive
        } = this.props

        return (
            <Cintillo imaResponsive={imaResponsive} imaDesktop={imaDesktop} />
        );
    }
}

CintilloMSIComponent.defaultProps = {
    imaResponsive: PATH_PORTAL + '/medios-plazavip/swift/v1/claroshop/cintillos/12_msi_tarjetas_de_credito/mobile.jpg?v=2.0',
    imaDesktop: PATH_PORTAL+ 'medios-plazavip/swift/v1/claroshop/cintillos/12_msi_tarjetas_de_credito/desktop.jpg?v=2.0'
}

export default CintilloMSIComponent;
