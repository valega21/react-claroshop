import React from "react";
import './cintilloMSI.sass';

const Cintillo = ({ imaResponsive, imaDesktop }) => {
    return <div className="cintillo-msi" id="cintillo-msi">
        <div className="cintillo-msi-img">
            <img alt="12 msi" className="msi-desktop" src={imaDesktop} />
            <img alt="12 msi" className="msi-mobile" src={imaResponsive} />
        </div>
    </div>
}

export default Cintillo;