import React, { Component } from 'react';
import SeccionLeftComponent from './sectionLeft';
import SeccionRightComponent from './sectionRight';
import { connect } from 'react-redux';
import getDetailProducto from '../../redux/actions/getDetailProducto';
import './detail.sass';

class DetailComponent extends Component {

    componentDidMount() {
        const { param } = this.props;
        this.props.getDetailProducto(param);
    }

    render() {
        const { dataProduct, param } = this.props;
        const producto = Object.values(Object.assign({}, dataProduct))
        const pro = []

        producto.map((ar) => {
            var carru = Object.entries(ar)
            carru.map((car, j) => {
                return (car[0] === "data"
                    ? pro.push({
                        id: car[j].id, title: car[j].title, description: car[j].description,
                        descuento: car[j].discount, precioVenta: car[j].sale_price,
                        precio: car[j].price, stock: car[j].stock, telmex: car[j].telmex_months,
                        brand: car[j].brand, store: car[j].store,
                        size_color: car[j].size_color,
                        images: car[j].images
                    })
                    : null
                )
            })
        })

        const secIzquierda = pro.map((item, i) => (
            <SeccionLeftComponent key={i}
                param={param} title={item.title} description={item.description}
                descuento={item.descuento} precioVenta={item.precioVenta} precio={item.precio}
                stock={item.stock} telmex={item.telmex} store={item.store}
                size_color={item.size_color} images={item.images}
            />
        ))

        const secDerecha = pro.map((item, i) => (
            <SeccionRightComponent key={i} brand={item.brand} store={item.store} />
        ))

        return (
            <section className="moduleFichaProducto">
                <div className="containerProducto">
                    {secIzquierda}
                    {secDerecha}
                </div>
            </section>

        );
    }
}

const mapStateToProps = (state) => {
    return {
        dataProduct: state.dataProduct
    }
}

const mapDispatchToProps = {
    getDetailProducto
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailComponent);