import React from 'react';

const ClaroEnvios = () => {
    return <div className="box_claroEnvios">
        <div className="ingresaCP"></div>
        <div className="resultadosCP show">

            <ul>
                <li id="delEncontrado">Enviar a</li>
                <li id="estafetaCodigoRojo">*Para este CP el producto se entregará en la oficina de Estafeta más cercana</li>
                <li className="enviosGratis">Envío gratis</li>
            </ul>

            <dl className="respuestaFecha">
                <dt>Fecha estimada: </dt>
                <dd id="fecha"> la fecha</dd>
            </dl>

            <p className="small">* Una vez autorizado el pago</p>
        </div>
    </div>
}

export default ClaroEnvios;