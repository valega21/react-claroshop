import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';

const Botones = () => {
    return <Fragment>
        <div className="cantProducto">
            <label>Cantidad</label>
            <input type="number" name="cantidad" id="cantidad" defaultValue="1" min="1" max="99" className="cantidadProduct"></input>
        </div>

        <div className="purchase_btns">
            <Link to="" className="btn-rojo" id="buyNow">Comprar Ahora</Link>
            <Link to="" className="btn-gris" id="addToCart">Agregar a Carrito</Link>

            <div className="moduleInfoSafe">
                <div className="icoDevGratis">Devolución gratis</div>
                <div className="icoCompSegura">Compra segura</div>
            </div>
        </div>
    </Fragment>
}

export default Botones; 