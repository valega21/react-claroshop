import React, { Component } from 'react';
import './seccionRight.sass';
import Botones from './botones';
import VendidoPor from './vendidoPor';
import VenderInternet from './venderPorInternet';
import ClaroEnvios from './claroEnvio';
import { PATH_PORTAL } from '../../../constants/endPoints';

class SeccionRightComponent extends Component {
    render() {
        const { store, url } = this.props

        return (
            <div className="purchaseModule">

                <Botones />

                <ClaroEnvios />

                <VendidoPor name={store.name} logo={store.logo} idTienda={store.id} />

                <VenderInternet url={url} />

            </div>
        );
    }
}

SeccionRightComponent.defaultProps = {
    url: PATH_PORTAL + '/medios-plazavip/swift/v1/claroshop/tagmanager/boton_vender_en_claroshop/grafico_cajas.png'
}

export default SeccionRightComponent;