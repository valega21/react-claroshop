import React from 'react';
import { Link } from 'react-router-dom';

const VenderInternet = ({url}) => {
    return <Link to="/l/vender-en-internet" className="vender-en-claroshop-a">
        <div className="vender-en-claroshop">
            <div className="vender-en-claroshop-img">
                <img alt="Vender en Claroshop" src={url} />
            </div>
            <div className="vender-en-claroshop-descripcion">
                <p className="vender-en-claroshop-title">Vender en Claroshop.com</p>
                {/* <p className="vender-en-claroshop-texto">Llega a millones de usuarios en México.</p> */}
            </div>
            <div className="arrowed">
                <div className="arrow-1"></div>
            </div>
        </div>
    </Link>
}

export default VenderInternet;