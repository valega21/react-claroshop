import React from 'react';
import { Link } from 'react-router-dom';

const VendidoPor = ({ name, logo, idTienda }) => {
    return <div className="moduleVendidoPor">
        <div className="componentLeft">
            <p>Vendido por: {name}</p>
            <img src={logo} alt={name} />
        </div>

        <div className="componentRight">
            <Link className="btn-gris" to={"/Tienda/" + idTienda}>Ir a la Tienda</Link>
        </div>
    </div>
}

export default VendidoPor;