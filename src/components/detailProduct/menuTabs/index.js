import React, { Component } from 'react';
import MenuTabs from './menuTabs';
import './menuTabs.sass';

class TabsContainer extends Component {
    render() {
        return (
            <div className="mod__DescripcionP">
                <MenuTabs description={this.props.description} />
            </div>
        );
    }
}

export default TabsContainer;