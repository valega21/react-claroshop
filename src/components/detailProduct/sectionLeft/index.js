import React, { Component } from 'react';
import DescriptionComponent from './description';
import SliderComponent from './slider';
import './index.sass';

class SeccionLeftComponent extends Component {
    render() {
        const { title, id, precio, precioVenta, descuento, param, description, stock, telmex, size_color, images } = this.props

        return (
            <div className="viewsNewDescrip">
                <SliderComponent images={images} title={title}/>

                <DescriptionComponent
                    param={param} title={title}
                    description={description}
                    descuento={descuento} precioVenta={precioVenta}
                    precio={precio} id={id}
                    stock={stock} telmex={telmex}
                    tallaColor={size_color}
                />
            </div>
        );
    }
}

export default SeccionLeftComponent;