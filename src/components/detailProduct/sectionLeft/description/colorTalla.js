import React, { Component, Fragment } from 'react'
// import $ from 'jquery'
import { connect } from 'react-redux';
import { cambiarColor, colorSeleccionado, cambiarTalla } from '../../../../redux/actions/getDetailProducto';

class ColorTalla extends Component {
    constructor(props) {
        super(props);
        this.state = {
            colorSeleccionadoSelect: this.props.tallaColor[0].color,// color seleccionado en el select
            tallaDefault: this.props.tallaColor[0].sizes[0].size,// this.props.tallaP,
        };
    }

    handleChangeColor = (event) => {
        this.props.cambiarColor(event.target.value);
        this.props.colorSeleccionado(event.target.value);
        this.props.cambiarTalla(this.state.tallaDefault)
        this.setState({ colorSeleccionadoSelect: event.target.value });
    }

    handleChangeTalla = (event) => {
        this.setState({ tallaDefault: event.target.value });
        this.props.cambiarTalla(event.target.value)
    }

    cambiarTallasSegunColor = (event) => {
        this.setState({ colorSeleccionadoSelect: event.target.value });
    }

    componentDidMount() {
        this.props.cambiarColor(this.state.colorSeleccionadoSelect);
    }
    render() {
        const { tallaColor } = this.props

        return (
            <Fragment>
                {/* ------ select COLOR -----*/}
                <div className="opciones">
                    <div className="boxColor">
                        <span>Color</span>
                        <div className="opc_colors">
                            <select name="color" id="color" onChange={this.handleChangeColor} onClick={this.cambiarTallasSegunColor}>

                                {tallaColor.map((prod, i) => {
                                    return (
                                        <option key={i} value={prod.color}> {prod.color} </option>
                                    );
                                })}
                            </select>
                        </div>
                    </div>

                    {/* ----- TALLA ----- */}
                    <div className="boxTallas">
                        <span>Tallas</span>
                        <div className="opc_sizes">
                            <select name="talla" id="talla" className="talla"
                                defaultValue={this.state.tallaDefault}
                                onChange={(e) => { this.handleChangeTalla(e) }}
                                onClick={this.skuSeleccionadoTalla} >

                                {tallaColor.map((tal) => {
                                    return (tal.sizes.map((ta, it) => {
                                        return (
                                            (tal.color === this.state.colorSeleccionadoSelect
                                                ? <option key={it} id={it} value={ta.size} >{ta.size} </option>
                                                : null
                                            )
                                        );
                                    }))
                                })}

                            </select>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        cambiarT: state.cambiarTalla,
        cambiarC: state.cambiarColor,
        colorSeleccionado: state.colorSeleccionado,
    }
}

export default connect(mapStateToProps, { cambiarTalla, cambiarColor, colorSeleccionado })(ColorTalla)