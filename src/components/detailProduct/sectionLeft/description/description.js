import React, { Component } from 'react';
import './description.sass';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { mostrarLista } from '../../../../redux/actions/getDetailProducto';
import ColorTalla from './colorTalla';

class Description extends Component {
    constructor(props) {
        super(props);
        this.state = {
            show: props.ver.mostrar
        }
    }

    componentDidMount() {
        this.props.mostrarLista(this.state.show);
    }

    handleClickShow = () => {
        this.setState({ show: !this.state.show })
    }

    render() {
        const { title, id, precio, precioVenta, descuento, param, description, stock, telmex, tallaColor } = this.props
        const { show } = this.state

        if (telmex.length > 0)
            var ultimo = telmex[telmex.length - 1];

        return (
            <div className="buy-Details">
                <h1>{title}</h1>

                {/* mensaje producto agotado */}
                {stock === 0
                    ? <div id="mensajeAgotado" className="mensajeAgotado">
                        <div className="alertaAgotado">
                            <span>Producto No disponible por el Momento</span>
                        </div>
                        <div className="paEnvio">
                            <div className="notificactionEmpty">
                                <div className="form-group">
                                    <input type="checkbox" id="checkAgotado" name="acepto[]" className="form-control :focus required" value />
                                    <label>Notificarme cuándo esté disponible</label>
                                </div>
                                <fieldset>
                                    <button id="sendMailNotif" className="seguirTienda" type="button">Aceptar</button>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                    : null
                }

                {/* mensaje enviar notificacion al estar disponible el producto*/}
                <div className="compartirEmail" id="compartirEmail2">
                    <div className="contElements">
                        <div className="contDescrip">
                            <p>Ingresa tu Correo para recibir Notificación cuando el producto este Disponible...</p>
                            <p>ejemplo@claroshop.com</p>
                        </div>
                    </div>
                    <form className="compartirEm">
                        <fieldset id="listaCorreosCat">
                            <input id="email" type="email" name="email"></input>
                            <h3 id="resultValid"></h3>
                        </fieldset>
                        <fieldset>
                            <button id="btnCatCompartida" className="btn-rojo" type="button">Enviar</button>
                        </fieldset>
                        <fieldset>
                            <p>o Si ya estas registrado : </p>
                            <button type="button" id="btnCatCompartida" className="btn-gris">Inicia Sesión</button>
                        </fieldset>
                    </form>
                </div>

                {/* lista de deseos */}
                <div className="content_rankwWish">
                    <span className="boxHeart btn-gris" id="addWishList" data-id={id}>
                        Agregar a Lista de Deseos
                </span>
                </div>

                {/* precio normal , precio con descuento, ahorro */}
                <div className="content_Precios">
                    {precio > precioVenta
                        ? <div className="antes">
                            <p> ${precio}</p>
                            {descuento > 0
                                ? <div className="descT">
                                    {descuento}%
                              </div>
                                : null
                            }
                        </div>
                        : null
                    }

                    <div className="total" id="precioCambio">
                        <p>
                            ${precioVenta}
                            <span>MXN</span>
                        </p>
                    </div>

                    {precio > precioVenta ? <p className="ahorra"> Ahorras: ${precio - precioVenta}</p> : null}
                </div>

                {/* promociones tarjetas de credito y telmex  */}
                {(telmex.length > 0) ?
                    <div className="content_Promociones">
                        <div className="circuloDesc envioGratis">
                            <p>Envío Gratis</p>
                        </div>

                        <dl className="tagMeses cardMeses">
                            <dt>
                                Hasta <strong> 12 </strong> meses sin intereses
                        <Link to="/claroPromo" className="link">Ver más</Link>
                            </dt>
                        </dl>

                        <dl onClick={this.handleClickShow} className={show === true ? "tagMeses telmex show" : " tagMeses telmex"}>
                            <dt>
                                <p>Hasta {ultimo.month} Meses de $ {ultimo.price}</p>
                            </dt>
                            <dd>
                                <ul>
                                    {telmex.map((item, index) => {
                                        return (
                                            <li key={index} >{item.month} Meses de $ {item.price} </li>
                                        )
                                    })}
                                    <li> Más costos de envio</li>
                                </ul>
                            </dd>

                            {stock <= 10
                                ? <div className="viewStock">
                                    Quedan
                                <span className="hotStock"> {stock} </span>
                                piezas en existencia
                               </div>
                                : null
                            }
                        </dl>

                    </div>
                    : null
                }

                {/* selects talla / color */}
                {tallaColor.length > 0
                    ?<ColorTalla tallaColor={tallaColor} />
                    : null
                }
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        ver: state.mostrarLista,
    }
}

function mapDispatchToProps(dispatch) {
    return ({
        mostrarLista: () => { dispatch(mostrarLista) }
    })
}

export default connect(mapStateToProps, mapDispatchToProps)(Description);