import React, { Component } from 'react';
import Description from './description';

class DescriptionComponent extends Component {
    render() {
        const { title, id, precio, precioVenta, descuento, param, description, stock, telmex, tallaColor } = this.props

        return (
            <Description param={param} title={title}
                description={description}
                descuento={descuento} precioVenta={precioVenta}
                precio={precio} id={id} stock={stock} telmex={telmex}
                tallaColor={tallaColor}
            />
        );
    }
}

export default DescriptionComponent;