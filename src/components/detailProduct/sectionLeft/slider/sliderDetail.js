import React, { Component, Fragment } from "react";
import { Link } from 'react-router-dom';
import './sliderDetail.sass';
import ModalZoom from './modal/modalZoomComponent'
import { connect } from 'react-redux';
import { mostrarSeleccion } from '../../../../redux/actions/getDetailProducto';
import $ from "jquery";

class SliderDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalIsOpen: false,
            imaActual: "",
            slideIndex: 0,
            indice: 0,
            inputPrueba: 0,
        };
        this.itemRefs = {};
    }

    openModal = (ima, i) => {
        this.setState({ modalIsOpen: !this.state.modalIsOpen });
        this.props.mostrarSeleccion(i);
        this.setState({ imaActual: ima });
    }

    closeModal = () => {
        this.setState({ modalIsOpen: false });
    }

    cambiarImagen = (event) => {
        this.setState({ imaActual: event.target.src });
    }

    recibir = (item) => {
        var indiceinput = document.getElementById('input1').value;
        const valor = parseInt(indiceinput)
        this.goToSlide(valor)
    }

    goToSlide = (index) => { this.setState({ slideIndex: index }); }

    goToPrevSlide = (e) => {
        e.preventDefault();

        let index = this.state.slideIndex;

        if (this.state.slideIndex > 0)
            --index;

        this.setState({ slideIndex: index });

        let desplazamiento = 30 * (-1);
        let pos = $('.vertical ').scrollTop() + desplazamiento;
        $('.vertical').animate({ scrollTop: pos }, 40)
    }

    goToNextSlide = (e) => {
        e.preventDefault();

        let index = this.state.slideIndex;
        let { images } = this.props;
        let slidesLength = images.length - 1;

        if (index < slidesLength)
            ++index;

        this.setState({ slideIndex: index });

        let desplazamiento = 30 * (1);
        let pos = $('.vertical ').scrollTop() + desplazamiento;
        $('.vertical').animate({ scrollTop: pos }, 40)
    }

    render() {
        const { images, title } = this.props

        return (
            <Fragment>
                <div className="bx-wrapper slider-for">
                    <div className="bx-viewport ">
                        <ul className="carrusel-producto slider-single" style={{ height: '438px' }}>
                            {images.map((ima, i) => {
                                return (

                                    <li key={i} aria-hidden="false" className={this.state.slideIndex !== i ? 'none' : ''}  >
                                        <img className="imagenS"
                                            id={i}
                                            onClick={(e) => this.openModal(ima.url, i)}
                                            alt={"imagen-producto-" + i}
                                            src={ima.url}
                                            data-slide-index={i}
                                        ></img>

                                        <ModalZoom
                                            title={title}
                                            imagenes={images}
                                            open={this.state.modalIsOpen}
                                            img={this.state.imaActual}
                                            cerrar={this.closeModal}
                                            recibir={this.recibir}
                                        />
                                    </li>
                                );
                            })}

                        </ul>
                    </div>
                </div>

                <button className="slick-prev1" type="button" onClick={e => this.goToPrevSlide(e)} />
                <div className="divVertical" style={{ height: '400px' }}>
                    <ul className="vertical" id="vertical" style={{ height: '370px', overflowX: 'hidden', overflowY: 'hidden' }} >
                        {images.map((ima, ind) => {
                            return (
                                <li key={ind} >
                                    <Link id="enlace" to="#!" style={{ width: '95%' }} onClick={e => this.goToSlide(ind)} >
                                        <img className={ind === this.state.slideIndex ? "active" : "normal"} id="imagen" data-slide-index={ind} alt={"imagen-producto-" + ind} src={ima.url} />
                                    </Link>
                                </li>
                            );
                        })}
                    </ul>
                </div>
                <button className="slick-next1" type="button" onClick={e => this.goToNextSlide(e)} />

                <input type="text" id="input1" defaultValue="" style={{ visibility: 'hidden' }} />

            </Fragment >
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        mostrar: state.mostrarSeleccion
    }
}

export default connect(mapStateToProps, { mostrarSeleccion })(SliderDetail)