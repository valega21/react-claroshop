import React, { Component } from "react";

export default class ListaComponent extends Component {

    envioPadre = (e) => {
        e.preventDefault();
        const dataIndex1 = e.target.getAttribute('data-slide-index');

        var indiceinput = document.getElementById('input1').value = dataIndex1;
        console.log("indice input1: ", indiceinput)
        this.props.indice(dataIndex1)
        const valor = parseInt(dataIndex1)
        // this.props.mostrarSeleccion(valor)
    }

    render() {
        const { ima, ind, isActive, onActiveTab, seleccion } = this.props

        return (
            <li className={isActive === true ? 'active' : ''}
                onClick={onActiveTab}
                aria-hidden="false"
                id={ind}
            >
                <img id={ind} data-slide-index={ind}
                    onClick={(e) => { seleccion(ima.thumbnail, ima.order); this.envioPadre(e); }}
                    alt="imagenes-producto" src={ima.thumbnail} />
            </li>
        );
    }
}