import React, { Component } from 'react';
import ReactModal from 'react-modal';
import './ModalComponent.sass';
import ListaComponent from './lista';
import { connect } from 'react-redux';
import { mostrarSeleccion } from '../../../../../redux/actions/getDetailProducto';

//estilos aplicados al modal
const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        width: '90vw',
        height: "100vh",
        transform: 'translate(-50%, -50%)',
        maxWidth: '1400px',
        maxHeight: '800px',
        backgroundColor: 'white',
        overflow: 'hidden'
    }
}

class ModalZoomComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            modalIsOpen: false, //bandera para abrir el modal
            claseZoom: false, // bandera para aplicar zoom
            seleccion: '',
            click: false,
            indice: 0,
            selectedTabId: -1, // variable para resaltar la imagen seleccionada del producto
            imagen: ""
        };
    }

    openModal = () => {
        this.setState({ modalIsOpen: true });
    }

    afterOpenModal = () => {
        this.subtitle.style.color = '#000';
    }

    zoom = () => {
        const currentState = this.state.claseZoom;
        this.setState({ claseZoom: !currentState });
    }

    seleccionImagen = (sel, ind) => {
        this.setState({ seleccion: sel })

        if (sel !== "") {
            this.setState({ click: true })
            // this.props.mostrarSeleccion(ind)
        }
    }

    indice = (ind) => {
        this.props.recibir(this.state.selectedTabId)
        console.log("funcion indice: ", ind)
        // this.props.mostrarSeleccion(ind)
    }

    componentDidMount() {
        this.props.cerrar(this.state.modalIsOpen)
    }

    UNSAFE_componentWillReceiveProps(prev) {
        this.setState({ selectedTabId: prev.mostrar.Imagenseleccionada })
        this.setState({ imagen: prev.img })
        // console.log("receive: ", prev)
    }

    isActive(id) {
        return this.state.selectedTabId === id
    }

    setActiveTab = (selectedTabId) => {
        this.setState({ selectedTabId });
    }

    render() {
        const { open, cerrar, imagenes, title } = this.props

        return (
            <ReactModal isOpen={open} onRequestClose={cerrar} style={customStyles} aria-modal="true" ariaHideApp={false}>
                <div className="bodyZoom">
                    <div className="contZoomRelative">
                        <button title="close" onClick={cerrar} className="closeZoom"> <span className="closeDes">&times;</span></button>

                        <div className="contentZoom" onClick={this.zoom}>
                            {imagenes.map((ima, id) => {
                                return (
                                    this.state.selectedTabId === id
                                        ? <img key={id} src={ima.url} alt={"imagen- " + id} className={this.state.claseZoom ? 'zoomin hover' : 'zoomin'} onClick={(e) => this.zoom(e)}
                                            style={{ height: '450px' }} />
                                        : null
                                );
                            })}

                        </div>

                        <div className="navigateZoom">
                            <h2 style={{ color: "#000" }}>{title}</h2>
                            <ul>
                                {imagenes.map((ima, id) => {
                                    return (
                                        <ListaComponent
                                            isActive={this.isActive(id)}
                                            onActiveTab={(e) => this.setActiveTab(id)}
                                            indice={this.indice}
                                            ind={id}
                                            key={id}
                                            ima={ima}
                                            seleccion={this.seleccionImagen}
                                            mostrarSeleccion={this.props.mostrarSeleccion}
                                        />
                                    );
                                })
                                }
                            </ul>
                        </div>
                    </div>
                </div>
            </ReactModal>
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        mostrar: state.mostrarSeleccion
    }
}

export default connect(mapStateToProps, { mostrarSeleccion })(ModalZoomComponent)