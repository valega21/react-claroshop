import React, { Component } from 'react';
import SliderDetail from './sliderDetail';

class SliderComponent extends Component {
    render() {
        const { images, title } = this.props;

        return (
            <div className="infoProduct ">
                <div className="media-container">
                    <SliderDetail images={images} title={title}/>
                </div>
            </div>
        );
    }
}

export default SliderComponent;