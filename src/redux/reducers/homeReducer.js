const defaultStore = {
    dataHomeProduct: []
}

function reducerHome(state = defaultStore, { type, payload }) {
    //console.log(action.payload)
    switch (type) {
        case 'addDataHomeProducts': {
            return Object.assign({}, state, { dataHomeProduct: payload })
        }
        default:
            return state;
    }
}

export default reducerHome;