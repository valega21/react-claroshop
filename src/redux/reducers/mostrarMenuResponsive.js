import { MOSTRAR } from '../actions/mostrarMenuResponsive';

const defaultState = {
    flag: false
};

function mostrarMenuResponsive(state = defaultState, action) {
    switch (action.type) {
        case MOSTRAR:
            return state= !state//Object.assign({}, state, { flag: action.payload });
        default:
            return state
    }
}

export default mostrarMenuResponsive;