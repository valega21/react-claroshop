const defaultStore = {
	menuCategoryHeader: []
};

function reducer(state = defaultStore, action){
	//console.log(action.payload)
	switch (action.type) {
		case 'addDataMenu':{
			return Object.assign({}, state, {menuCategoryHeader: action.payload.data});
		}
		default:{
			return state;
		}
	}
}

export default reducer;