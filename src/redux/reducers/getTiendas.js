import { GETDATA} from '../actions/getTiendas';

const defaultStore = {
    listatiendas: []
}

function reducerTiendas(state = defaultStore, {type, payload}) {
    switch (type) {
        case GETDATA: {
            return Object.assign({}, state, { listatiendas: payload});
        }
        default:
            return state;
    }
}

export default reducerTiendas