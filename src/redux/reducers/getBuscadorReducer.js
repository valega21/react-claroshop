import { FETCH_BUSQUEDA_REQUEST, FETCH_BUSQUEDA_SUCCESS, FETCH_BUSQUEDA_ERROR } from '../actions/getBuscadorAction';

const defaultState = {
    loading: false,
    producto: [],
    error: ''
};

function reducerGetBuscador(state = defaultState, { type, payload }) {
    //console.log(action.payload)
    switch (type) {
        case FETCH_BUSQUEDA_REQUEST: {
            return {
                ...state,
                loading: true
            }
        }
        case FETCH_BUSQUEDA_SUCCESS: {
            return{
                loading: false,
                producto: payload,
                error: ''
            }
        }
        case FETCH_BUSQUEDA_ERROR: {
            return{
                loading: false,
                producto: [],
                error: payload
            }
        }
        default:
            return state;
    }
}

export default reducerGetBuscador;