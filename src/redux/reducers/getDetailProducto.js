import { MOSTRARLISTA, MOSTRARTAB } from '../actions/getDetailProducto';
import { CAMBIAR_COLOR, COLOR_SELECCIONADO } from '../actions/getDetailProducto';
import { CAMBIAR_TALLA } from '../actions/getDetailProducto';

const defaultStateTalla = {
    talla: ""
};

const defaultStateColor = {
    color: "",
    cambiarColor: ""
};
const defaultStore = {
    dataProduct: []
};
const defaultState = {
    mostrar: false
}
const defaultTab = {
    Imagenseleccionada: 0
}

function reducerDetailProducto(state = defaultStore, { type, payload }) {
    //console.log(action.payload)
    switch (type) {
        case 'addDataProduct': {
            return Object.assign({}, state, { dataProduct: payload });
        }
        default:
            return state;
    }
}

export function mostrarLista(state = defaultState, action) {
    switch (action.type) {
        case MOSTRARLISTA:
            return state = !state
        default:
            return state
    }
}

export function mostrarSeleccion(state = defaultTab, {type, payload}) {
    switch (type) {
        case MOSTRARTAB:
            return Object.assign({}, state, { Imagenseleccionada: payload });
        default:
            return state
    }
}

export function cambiarColor(state = defaultStateColor, { type, payload }) {
    switch (type) {
        case CAMBIAR_COLOR:
            return Object.assign({}, state, { color: payload });
        default:
            return state
    }
}

export function colorSeleccionado(state = defaultStateColor, { payload, type }) {
    switch (type) {
        case COLOR_SELECCIONADO:
            return Object.assign({}, state, { cambiarColor: payload });
        default:
            return state
    }
}

export function cambiarTalla(state = defaultStateTalla, { type, payload }) {
    switch (type) {
        case CAMBIAR_TALLA:
            return Object.assign({}, state, { talla: payload });
        default:
            return state
    }
}

export default reducerDetailProducto;