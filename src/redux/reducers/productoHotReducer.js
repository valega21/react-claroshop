import { ADDPRODUCTOHOT} from '../actions/getProductoHot';

const defaultStore = {
    productoHot: []
}

export function reducerProductoHot(state = defaultStore, {type, payload}) {
    switch (type) {
        case ADDPRODUCTOHOT: {
            return Object.assign({}, state, { productoHot: payload.data});
        }
        default:
            return state;
    }
}

// export default reducerProductoHot;