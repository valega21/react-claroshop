import axios from 'axios';
import { AJAX_TOKEN } from '../../constants/endPoints';

const addToken = () =>{
    const URL = AJAX_TOKEN;
    const Options = {
        method: 'get',
        url: URL,
        withCredentials: true,
        credentials: 'same-origin',
        headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json' 
        }
    }
    
    return(
        axios(Options)
        .then((res) =>{console.log(res)})
        .catch((err) =>{console.log(err)})
    ) 

}

export default addToken;