import axios from 'axios';
import { MENU_PORTAL } from '../../constants/endPoints';

const menuPortalData = () =>{
	const URL = MENU_PORTAL;
	const Options = {
        method: 'get',
        url: URL,
        headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json' 
        }
    }
	
	return async (dispatch, getState) => {
		await axios(Options)
		.then((res) => {
			dispatch({ 
				type: 'addDataMenu',
				payload: res.data
			});
		})
		.catch((err) =>
			console.error(err)
		)
	};
}

export default menuPortalData;