import axios from 'axios';
import { SEND_NEWSLETTER } from '../../constants/endPoints';

const sendNewsletter = dataNews =>{
    const URL = SEND_NEWSLETTER;
    const Options = {
        method: 'post',
        url: URL,
        data: JSON.stringify(dataNews),
        headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json' 
        }
    }

    return async (dispatch, getState) =>(
        await axios(Options)
        .then((res) =>{
            dispatch({ 
				type: 'addToken',
				payload: res.data
            });
            console.log(res.data)
        })
        .catch((err) => {
            console.error(err)
        })
    )
                     
}

export default sendNewsletter;