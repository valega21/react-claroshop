import { SEARCH_ANTEATER, PRODUCTS_PER_PAGINATION } from '../../constants/endPoints';
import axios from 'axios'
export const FETCH_BUSQUEDA_REQUEST = 'FETCH_BUSQUEDA_REQUEST';
export const FETCH_BUSQUEDA_SUCCESS = 'FETCH_BUSQUEDA_SUCCESS';
export const FETCH_BUSQUEDA_ERROR = 'FETCH_BUSQUEDA_ERROR';

//ACCIONES
export default async function getSearch({ searchText = '', brand = '', higherPrice = '', lowerPrice = '', id_Category = '', order = '', pagination = '' } = {}) {

    var prices, brands, byOreder, byCategory, dots, byPagination

    brand && higherPrice && lowerPrice
        ? dots = `.`
        : dots = ''

    higherPrice && lowerPrice
        ? prices = `price>${lowerPrice}.price<${higherPrice}`
        : prices = ''
    brand
        ? brands = `attribute_marca:${brand}`
        : brands = ''
    order
        ? byOreder = `meta:sale_price:${order}`
        : byOreder = ''
    id_Category
        ? byCategory = `categories->id:${id_Category}`
        : byCategory = ''
    pagination
        ? byPagination = pagination - 1
        : byPagination = 0
    // console.log(" respuesta ---------------->", searchText, brand, higherPrice, lowerPrice, id_Category, order)
    const apiURL = SEARCH_ANTEATER + `?cliente=claro&proxystylesheet=xml2json&oe=UTF-8&getfields=*&sort=${byOreder}&start=${byPagination * PRODUCTS_PER_PAGINATION}&num=${PRODUCTS_PER_PAGINATION}&q=${searchText}&requiredfields=${brands}${dots}${prices}&ds=marcas:attribute_marca:0:0:8:0:1.sale_precio:sale_price:1:1:1000&do=categories:categories:id,name:10:1&requiredobjects=${byCategory}`

    const config = {
        method: 'get',
        url: apiURL,
        timeout: 5000,
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json; charset=utf-8'
        }
    };


    return await axios(config)
        .then(res => {
            const {
                //config,
                //headers, 
                status,
                data
            } = res
            //console.log(res)
            // console.log(config)
            //console.log(data)
            // console.log(headers)
            // console.log(status)

            const searchResult = data
            if (status === 200) {
                if (!Array.isArray(searchResult)) {
                    return { searchResult }
                }
                else {
                    return { 'error': status, 'response': data }
                }
            }
            return []
        })
        .catch(error => {
            //console.log(error.request.status)
            if (error.message === 'Network Error') {
                console.log('Error de conexión')
            }
            else if (error.request) {
                console.log(error.request.responseText)
            }
            if (error.request.status === 0) {
                console.log('request 0')
            } else {
                if (error.response.status === 401) {
                    console.log(error.response.data.msg)
                    return { error: error.response.status, message: error.response.data.msg }
                }
                else if (error.response.status === 404) {
                    console.error(error.response);
                    console.log('request 404')
                }
                else if (error.response.status === 440) {
                    console.log('request 440')
                }
                else if (error.response.status === 500) {
                    console.error(error.response);
                    console.log('request 500')
                }
                else {
                    console.error(error.response.status);
                    console.error(error.response);
                }
            }
        })
}
