import axios from 'axios';
import { GET_HOME } from '../../constants/endPoints';

const getDataHomeProd = () => {
    const URL = GET_HOME;
    const Options = {
        method: 'get',
        url: URL,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }

    return async (dispatch, getState) => {
        await axios(Options)
            .then((res) => {
                dispatch({
                    type: 'addDataHomeProducts',
                    payload: res.data
                });
                //console.log(res.data)
            })
            .catch((err) => {
                console.error(err)

            })
    };
}

export default getDataHomeProd;