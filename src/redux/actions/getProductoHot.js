import axios from 'axios';
import { PRODUCT_HOT } from '../../constants/endPoints';
export const ADDPRODUCTOHOT = 'ADDPRODUCTOHOT'

const productoHotData = () =>{
	const URL = PRODUCT_HOT;
	const Options = {
        method: 'get',
        url: URL,
        headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json' 
        }
    }
	
	return async (dispatch, getState) => {
		await axios(Options)
		.then((res) => {
            
			dispatch({ 
				type: ADDPRODUCTOHOT,
				payload: res.data
			});
		})
		.catch((err) =>
			console.error(err)
		)
	};
}

export default productoHotData;