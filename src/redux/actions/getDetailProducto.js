import axios from 'axios';
import { DETAIL_PRODUCT } from '../../constants/endPoints';
export const MOSTRARLISTA = 'MOSTRARLISTA'
export const CAMBIAR_COLOR = 'CAMBIAR_COLOR';
export const COLOR_SELECCIONADO = 'COLOR_SELECCIONADO'
export const CAMBIAR_TALLA = 'CAMBIAR_TALLA';
export const MOSTRARTAB = 'MOSTRARTAB';

const addDataProduct = id => {
    const URL = DETAIL_PRODUCT + id;
    //console.log(URL)

    return async (dispatch, getState) => {
        await axios.get(URL)
            .then((res) => {
                dispatch({
                    type: 'addDataProduct',
                    payload: res.data
                });
            })
            .catch((err) =>
                console.log(err)
            )
    };
}

export const mostrarLista = (mostrar) => {
    return {
        type: MOSTRARLISTA
    }
}

export function cambiarColor(color) {
    return (dispatch) => {
        dispatch({ type: CAMBIAR_COLOR, payload: color })
    }
}

export function colorSeleccionado(evento) {
    return (dispatch) => {
        dispatch({ type: COLOR_SELECCIONADO, payload: evento })
    }
}

export function cambiarTalla(talla) {
    return (dispatch) => {
        dispatch({ type: CAMBIAR_TALLA, payload: talla })
    }
}

export function mostrarSeleccion( Imagenseleccionada) {
    return (dispatch) => {
        dispatch({ type: MOSTRARTAB, payload: Imagenseleccionada })
    }
}

export default addDataProduct;