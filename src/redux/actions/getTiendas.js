import axios from 'axios';
import { TIENDAS } from '../../constants/endPoints';
export const GETDATA = 'GETDATA'

const tiendasData = () =>{
	const URL = TIENDAS;
	const Options = {
        method: 'get',
        url: URL,
        headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json' 
        }
    }
	
	return async (dispatch, getState) => {
		await axios(Options)
		.then((res) => {
            
			dispatch({ 
				type: GETDATA,
				payload: res.data
			});
		})
		.catch((err) =>
			console.error(err)
		)
	};
}

export default tiendasData;