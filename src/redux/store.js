import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from "redux-thunk";

/*Devtools*/
import { composeWithDevTools } from 'redux-devtools-extension';

/*Reducers*/
import menuCategoryHeader from './reducers/headerReducer';
import mostrarMenuResponsive from './reducers/mostrarMenuResponsive';
import { reducerProductoHot } from './reducers/productoHotReducer';
import dataHomeProduct from './reducers/homeReducer';
import dataProduct from './reducers/getDetailProducto';
import { mostrarSeleccion, mostrarLista } from './reducers/getDetailProducto';
import { cambiarColor, colorSeleccionado, cambiarTalla } from './reducers/getDetailProducto'
import reducerTiendas from './reducers/getTiendas';
import reducerGetBuscador from './reducers/getBuscadorReducer';

const reducer = combineReducers({
	menuCategoryHeader,
	mostrarMenuResponsive,
	productoHot: reducerProductoHot,
	dataHomeProduct,
	dataProduct,
	mostrarLista,
	mostrarSeleccion,
	cambiarColor,
	colorSeleccionado,
	cambiarTalla,
	reducerTiendas,
	reducerGetBuscador
});


const store = createStore(
	reducer,
	composeWithDevTools(
		applyMiddleware(thunk)
	)
);

export default store;