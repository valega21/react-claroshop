import React, { Component } from 'react';
import { Switch, Route, Router} from 'react-router-dom';
import './Claroshop.sass';
import 'bulma/css/bulma.css'
import Home from './pages/home';
import Detail from './pages/detail';
import { NotFound } from './pages/notFound';
import Tiendas from './pages/tiendas';
import TiendaSeleccionadaPage from './pages/tiendaSeleccionada';
import Resultados from './pages/resultados';
import ResultadosCategoria from './pages/resultadosCategoria';
import history from './history'

class Claroshop extends Component {

  render() {
    return (
      <div className="App">
        <Router history={history}>
          <Switch>
            <Route exact path="/" component={Home} />

            <Route path='/producto/:id' render={(props) => <Detail {...props} />} />
            
            <Route exact path='/tiendas' component={Tiendas} />
            <Route path='/resultados/q=:busqueda' render={(props) => <Resultados {...props} />} />
            <Route path="/tienda/:id/:marca" render={(props) => <TiendaSeleccionadaPage {...props} />} />
            <Route path="/categoria/:id/:nombreCategoria"  render={(props) => <ResultadosCategoria {...props} />} />
            <Route component={NotFound} />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default Claroshop;
