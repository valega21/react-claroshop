//Config
export const PRODUCTION = 'https://www.claroshop.com/';
export const PRODUCTION_API = 'https://csapi.claroshop.com/';
export const PRODUCTION_RES = 'https://resources.claroshop.com/';

export const RELEASE = 'https://www.release.claroshop.com/';
export const RELEASE_API = 'https://csapi.release.claroshop.com/';
export const RELEASE_RES = 'https://resources.release.claroshop.com/';

export const DEVELOPER = 'https://www.beta.dev.claroshop.com/';
export const DEVELOPER_API = 'https://csapi.dev.claroshop.com/';
export const DEVELOPER_RES = 'https://resources.dev.claroshop.com/';

export const PATH_PORTAL = RELEASE;
export const PATH_API = PRODUCTION_API//RELEASE_API;
export const RESOURCES = RELEASE_RES;

//export const MENU_PORTAL = PATH_API + 'app/ApiRest/Categorias/'; https://www.release.claroshop.com/app/ApiRest/Home/
export const MENU_PORTAL = PATH_API + 'producto/categoria';

export const SEARCH_ANTEATER = 'https://claroshop.anteater.ai/search';
export const SEARCH_AUTOCOMPLETE = 'https://claroshop.anteater.ai/autocompletar';
export const STAR_YOTPO = 'https://api.yotpo.com/v1/widget/ODCmlTBzdqbZ6ElVDgdVdOSxTsRyjngIpblOBtgx/products/';

// export const GET_HOME = PATH_API + 'app/ApiRest/Home/';
export const GET_HOME = DEVELOPER_API + 'app/ApiRest/Home';
export const GET_HOME_EVOL = 'https://f12studio.com/CLARO89556546/api_claro/getHome/';
export const GET_PRODUCT = PATH_API + 'producto/';
export const GET_CATEGORY = PATH_API + 'app/ApiRest/Categorias?id=';

export const SEND_NEWSLETTER = PATH_PORTAL + 'suscribirseBoletin/';

export const AJAX_LOGIN = PATH_PORTAL + 'login/login/checkLogin?tklgn=';

export const AJAX_TOKEN = PATH_PORTAL + 'carrito/getAjaxToken';
export const GET_CART = PATH_PORTAL + 'carrito/obtener';
export const GET_ADD_PRODUCT_CART = PATH_PORTAL + 'carrito/agregar';
export const GET_REMOVE_PRODUCT_CART = PATH_PORTAL + 'carrito/eliminar';
export const GET_UPDATE_PRODUCT_CART = PATH_PORTAL + 'carrito/actualizarCantidad';
export const GET_SHIPPING = PATH_API + '/shipping/v1/zoning/';

// producto hot en home
export const PRODUCT_HOT = PATH_API + 'producto/1245576'; //+ id 1245576

//detalle de producto
export const DETAIL_PRODUCT = DEVELOPER_API + 'app/v1/product/'// 727162'
// export const DETAIL_PRODUCT = PRODUCTION_API + 'producto/' 

// pagina tiendas oficiales
export const TIENDAS = PRODUCTION_API + 'app/ApiRest/TiendasService' //https://csapi.claroshop.com/app/ApiRest/TiendasService

export const PRODUCTS_PER_PAGINATION= 100;
export const NO_IMAGE = PRODUCTION + '/medios-plazavip/swift/v1/assets/img/noImage.png';
export const HIDDEN_DISCOUNT = 10;
export const ENVIO_GRATIS = 499;