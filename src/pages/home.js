import React, { Component, Fragment } from 'react';
import MovieList from '../components/movieList';
import Header from '../components/header/header';
import Footer from '../components/footer';
import HomeComponent from '../components/home';

class Home extends Component {

    state = {
        useSearch: false,
        results: []
    }

    handleResults = (results) => {
        this.setState({ results, useSearch: true })
    }

    renderResults() {

        return this.state.results.length === 0
            ? null
            : <MovieList results={this.state.results} />
    }

    render() {
        return (
            <Fragment>
                <Header onResult={this.handleResults} />
                <HomeComponent/>
                    <div style={{ marginTop: '50px', marginBottom: '50px' }}>
                    {this.state.useSearch
                        ? this.renderResults()
                        : null
                    }
                </div>
                <Footer />
            </Fragment>
        )
    }
}

export default Home;