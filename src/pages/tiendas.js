import React, { Component, Fragment } from 'react';
import Footer from '../components/footer';
import Header from '../components/header/header';
import TiendasComponent from '../components/tiendas';

class Tiendas extends Component {
    render() {
        return (
            <Fragment>
                <Header />
                <TiendasComponent />
                <Footer />
            </Fragment>

        )
    }
}

export default Tiendas;