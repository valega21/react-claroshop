import React, { Fragment } from 'react'
import { ButtonBack } from '../components/botonBack'
import Header from '../components/header/header'
import Footer from '../components/footer'

export const NotFound = () => {
    return <Fragment>
        <Header />

        <div style={{ textAlign: 'center', marginTop:'30px', marginBottom:'30px' }}>
            <ButtonBack />
            <h1 className="title" style={{ marginTop: "50px" }}> 404</h1>
            <h2 className="subtitle"> Página no encontrada!</h2>
        </div>

        <Footer />
    </Fragment>
}