import React, { Component, Fragment } from 'react';
import Footer from '../components/footer';
import Header from '../components/header/header';

class PageResultadosCategoria extends Component {
    render() {
        console.log("pagina resultados -> ", this.props.history)
        return (
            <Fragment>
                <Header />
                <h1>Resultados de busqueda Categoria</h1>
                <Footer />
            </Fragment>

        )
    }
}

export default PageResultadosCategoria;