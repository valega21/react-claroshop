import React, { Component, Fragment } from 'react';
import Footer from '../components/footer';
import Header from '../components/header/header';
import BusquedaProducto from '../components/busqueda/busquedaProducto';

class PageResultados extends Component {
    render() {

        return (
            <Fragment>
                <Header />
                <BusquedaProducto  param={this.props.match.url} />
                <Footer />
            </Fragment>

        )
    }
}

export default PageResultados;