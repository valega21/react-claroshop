import React, { Component, Fragment } from 'react';
import Footer from '../components/footer';
import Header from '../components/header/header';
import DetailComponent from '../components/detailProduct';
// const API_KEY = '53de9c96';

class Detail extends Component {

    // state = {
    //     movie: {}
    // }

    // fechMovie = ({ id }) => {
    //     var url = `http://www.omdbapi.com/?apikey=${API_KEY}&i=${id}`
    //     fetch(url)
    //         .then(respuesta => respuesta.json())
    //         .then(movie => {
    //             this.setState({ movie })
    //         })
    // }

    render() {
        // const { Title, Poster, Actors, Metascore, Plot } = this.state.movie

        return (
            <Fragment>
                <Header />
                <DetailComponent param={this.props.match.params.id} />
                <Footer />
            </Fragment>

        )
    }
}

export default Detail;