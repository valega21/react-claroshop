import React, { Component, Fragment } from 'react';
import Footer from '../components/footer';
import Header from '../components/header/header';
import TiendaSeleccionadaComponent from '../components/tiendas/tiendaSeleccionada';

class TiendaSeleccionadaPage extends Component {
    render() {
        return (
            <Fragment>
                <Header />
                <TiendaSeleccionadaComponent param={this.props.match.params} />
                <Footer />
            </Fragment>

        )
    }
}

export default TiendaSeleccionadaPage;